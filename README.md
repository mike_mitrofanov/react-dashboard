# Requirements

* NodeJS
* MongoDB
* Npm

# Using

* run **npm install** from both **backend** and **frontend** folders before starting your local servers
* run **npm start** from **backend** folder to run NodeJS backend server. Server will start on 3000 port by default
* run **npm start** from **frontend** folder to run webpack-dev-server. Server will start on 8081 port by default

Repository has postman collection in root folder.
This collection describes backend server routes

# Add TSLint
## https://palantir.github.io/tslint/
yarn add tslint typescript
## enable in WebStorm
File -> Settings -> Languages & Frameworks -> TypeScript -> TSLint: Enable
