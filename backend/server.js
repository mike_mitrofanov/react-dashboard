const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Promise = require('bluebird');
const bodyParser = require('body-parser');
const cors = require('cors');
require('./websocket.server');

//config
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
Promise.onPossiblyUnhandledRejection((e, promise) => {});

//mongo config
mongoose.connect('mongodb://localhost:27017/dashboard', (err, db) => {
    if (err) {
        throw err;
    }
});
process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.error('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

//create server
const server = app.listen(3000, () => {
    console.log('listening on 3000')
});
server.on('close', () => {
    mongoose.disconnect();
});

/////////////////////////////////
//load routes
require('./modules')(app);
app.get('/', (req, res) => {
    res.send('Test');
});
