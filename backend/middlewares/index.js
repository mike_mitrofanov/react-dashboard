(function() {
    'use strict';

    const _ = require('lodash');

    const userModel = require('../modules/user/model');
    const boardModel = require('../modules/board/model');

    const tokenMiddleware = (req, res, next) => {
        if (typeof req.get('Authorization') === 'undefined') {
            res.status(401)
                .json({
                    success: false,
                    msg: 'Not authorized'
                });
            return;
        }

        userModel.validateByToken(req, req.get('Authorization').replace(/Bearer\s/, ''))
            .then((user) => {
                next();
            })
            .catch((error) => {
                res.status(401).json({
                    success: false,
                    msg: error.errmsg
                });
            });
    };

    const boardAvailableMiddleware = (req, res, next) => {
        boardModel.getBoards(req)
            .then((result) => {
                const board = _.find(result, (item) => {
                    return item._id.toString() === req.params.id.toString();
                });

                if (typeof board !== 'undefined') {
                    req.board = board;
                    next();
                } else {
                    res.status(401).json({
                        success: false,
                        msg: 'Can not get board'
                    });
                }
            })
            .catch((error) => {
                res.status(401).json({
                    success: false,
                    msg: 'Can not get board'
                });
            });
    };

    const isUserBorderOwner = (req, res, next) => {
       if (req.board.owner._id.toString() === req.user._id.toString()) {
           next();
       } else {
           res.status(401).json({
               success: false,
               msg: 'You do not have access to this board'
           });
       }
    };

    module.exports = {
        tokenMiddleware,
        boardAvailableMiddleware,
        isUserBorderOwner
    }
})();