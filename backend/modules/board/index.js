(function() {
    const _ = require('lodash');

    const middlewares = require('../../middlewares');
    const boardModel = require('./model');
    const userModel = require('../user/model');
    const wsServer = require('../../websocket.server');
    const helpers = require('../../helpers');

    const sendError = (res, message) => {
        res.status(500).json({
            msg: message
        });
    };

    const getBoards = (app) => {
        app.get('/boards', middlewares.tokenMiddleware, (req, res, next) => {
            helpers.promiseAllToObject({
                boards: boardModel.getBoards(req),
                users: userModel.getUsers({query:{search: req.user.email}})
            })
                .then((result) => {
                    const { boards, users } = result;
                    // sorts board according user preferences
                    if (users[0].settings && users[0].settings.boardIds) {
                        for (let i = users[0].settings.boardIds.length - 1; i >= 0; i--) {
                            let id = users[0].settings.boardIds[i];
                            let _boards = _.remove(boards, item => item._id == id); // need exactly == to convert item._id to string
                            if (_boards.length) {
                                boards.splice(0, 0, _boards[0]);
                            }
                        }
                    }
                    res.status(200).json(boards);
                })
                .catch((error) => {
                    sendError(res, (error.errmsg) ? error.errmsg : error.message);
                });
        });
    };

    const getBoard = (app) => {
        app.get('/boards/:id', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            res.status(200).json(req.board);
        });
    };

    const createBoard = (app) => {
        app.post('/boards', middlewares.tokenMiddleware, (req, res, next) => {
            boardModel.createBoard(req)
                .then((result) => {
                    res.status(200).json(result)
                })
                .catch((error) => {
                    sendError(res, (error.errmsg) ? error.errmsg : error.message);
                });
        });
    };

    const updateBoard = (app) => {
        app.put('/boards/:id',
            middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, middlewares.isUserBorderOwner,
            (req, res, next) => {
                if (typeof req.body.name === 'undefined') {
                    sendError(res, 'Can not update board');
                    return;
                }

                boardModel.model.update({_id: req.board._id}, req.body, (err, result) => {
                    if (err) {
                        sendError(res, (err.errmsg) ? err.errmsg : 'Can not update board');
                    } else {
                        res.status(200).json({
                            success: true,
                            msg: 'Board has updated'
                        });
                    }
                });
            }
        );
    };

    const updateColumnsOrder = (app) => {
        app.put('/boards/:id/columnsOrder',
            middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, middlewares.isUserBorderOwner,
            (req, res, next) => {
                if (typeof req.body.columnIds === 'undefined') {
                    sendError(res, 'Can not update columns order');
                    return;
                }

                boardModel.model.find({_id: req.board._id})
                    .then((boards) => {
                        if (!boards || !boards.length) {
                            return Promise.reject({errmsg: 'Error in getting boards'})
                        }

                        const _board = boards[0];
                        const _columns = JSON.parse(JSON.stringify(_board.columns));
                        _board.columns.length = 0;
                        req.body.columnIds.map(id => {
                            _board.columns.push(_.remove(_columns, (column) => column._id == id)[0]);
                        });
                        delete _board._id;

                        boardModel.model.update({_id: req.board._id}, _board, (err, result) => {
                            if (err) {
                                sendError(res, (err.errmsg) ? err.errmsg : 'Can not update board');
                            } else {
                                res.status(200).json({
                                    success: true,
                                    msg: 'Board has updated'
                                });
                            }
                        });
                    })
                    .catch((error) => {
                        return Promise.reject(error)
                    });
            }
        );
    };

    const deleteBoard = (app) => {
        app.delete('/boards/:id',
            middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, middlewares.isUserBorderOwner,
            (req, res, next) => {

                req.board.assigners.forEach((assigner) => {
                    wsServer.broadcast({
                        type: wsServer.USER_UNASSIGNED_FROM_BOARD,
                        id: assigner._id.toString()
                    });
                });

                boardModel.model.remove({_id: req.board._id}, (err, result) => {
                    if (err) {
                        sendError(res, (err.errmsg) ? err.errmsg : 'Can not delete board');
                    } else {
                        res.status(200).json({
                            success: true,
                            msg: 'Board deleted'
                        })
                    }
                });
            }
        );
    };

    const assignUser = (app) => {
        app.put('/boards/:id/assignUser',
            middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, middlewares.isUserBorderOwner,
            (req, res, next) => {
                if (typeof req.body.user_id === 'undefined') {
                    sendError(res, 'Can not assign user');
                    return;
                }

                if (_.findIndex(req.board.assigners, (item) => item._id.toString() === req.body.user_id.toString()) === -1) {
                    req.board.assigners.push(req.body.user_id);
                }

                req.board.save()
                    .then((error, result) => {
                        wsServer.broadcast({
                            type: wsServer.USER_ASSIGNED_TO_BOARD,
                            id: req.body.user_id
                        });
                        res.status(200).json({
                            success: true,
                            msg: 'User was assigned'
                        });
                    })
                    .catch((error) => {
                        sendError(res, error.errmsg ? error.errmsg : 'Error in assign user');
                    });
            }
        );
    };

    const unassignUser = (app) => {
        app.put('/boards/:id/unassignUser',
            middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, middlewares.isUserBorderOwner,
            (req, res, next) => {
                if (typeof req.body.user_id === 'undefined') {
                    sendError(res, 'Can not remove user from board');
                    return;
                }

                const index = _.findIndex(req.board.assigners, (item) => item._id.toString() === req.body.user_id.toString());
                if (index === -1) {
                    sendError(res, 'User not found');
                    return;
                }

                req.board.assigners.splice(index, 1);

                req.board.save()
                    .then((error, result) => {
                        wsServer.broadcast({
                            type: wsServer.USER_UNASSIGNED_FROM_BOARD,
                            id: req.body.user_id
                        });
                        res.status(200).json({
                            success: true,
                            msg: 'User was unassigned'
                        });
                    })
                    .catch((error) => {
                        sendError(res, error.errmsg ? error.errmsg : 'Error in removing user from assigners');
                    });
            }
        );
    };

    module.exports = (app) => {
        getBoards(app);
        getBoard(app);
        createBoard(app);
        updateBoard(app);
        updateColumnsOrder(app);
        deleteBoard(app);
        assignUser(app);
        unassignUser(app);
    }
})();
