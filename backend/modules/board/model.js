(function() {
    'use strict';

    const monogodb = require('mongodb');
    const mongoose = require('mongoose');
    const Promise = require('bluebird');
    const deepPopulate = require('mongoose-deep-populate')(mongoose);

    const boardSchema = new mongoose.Schema({
        name: {type: String, required: true},
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users',
            required: true
        },
        assigners: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        }],
        updated: { type: Date, default: Date.now },
        created: { type: Date, default: Date.now },

        columns: [{
            name: {type: String, required: true},
            tickets: [{
                title: {type: String, required: true},
                description: {type: String, default: ''},
                assigners: [{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'users'
                }],
                creator: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'users',
                    required: true
                },
                comments: [{
                    text: {type: String, required: true},
                    user: {
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'users',
                        required: true
                    }
                }]
            }]
        }]
    });
    boardSchema.plugin(deepPopulate);
    const boardModel = mongoose.model('boards', boardSchema);

    const createBoard = (req) => {
        return new boardModel({
            name: req.body.name,
            owner: req.user._id
        }).save()
            .then((result) => {
                if (!result) {
                    return Promise.reject({errmsg: 'Error in creating board'});
                }

                return boardModel.findById(result._id).populate('owner', 'email');
            })
            .then(item => {
                return Promise.resolve(item);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    };

    const getBoards = (req) => {
        return boardModel.find({$or: [{owner: req.user._id}, {assigners: req.user._id}]})
            .populate('owner', 'email firstName lastName thumbnail')
            .populate('assigners', 'email firstName lastName thumbnail')
            .populate('columns.tickets.creator', 'email firstName lastName thumbnail')
            .populate('columns.tickets.assigners', 'email firstName lastName thumbnail')
            .populate('columns.tickets.comments.user', 'email firstName lastName thumbnail')
            .then((result) => {
                if (!result) {
                    return Promise.reject({errmsg: 'Error in getting boards'})
                }

                return Promise.resolve(result);
            })
            .catch((error) => {
                return Promise.reject(error)
            });
    };

    module.exports = {
        createBoard,
        getBoards,

        model: boardModel
    }
})();
