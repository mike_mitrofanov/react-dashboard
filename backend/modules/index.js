(function() {
    'use strict';

    module.exports = (app) => {
        require('./user')(app);
        require('./board')(app);
        require('./column')(app);
        require('./ticket')(app);
        require('./comment')(app);
    }
})();