(function() {
    'use strict';

    const monogodb = require('mongodb');
    const mongoose = require('mongoose');
    const Promise = require('bluebird');
    const passwordHash = require('password-hash');
    const jwt = require('jsonwebtoken');
    const deepPopulate = require('mongoose-deep-populate')(mongoose);

    const userSchema = new mongoose.Schema({
        email: {type: String, required: true, unique: true},
        password: {type: String, required: true},
        verified: {type: Boolean, default: false},
        token: {type: String},
        firstName: {type: String},
        lastName: {type: String},
        phone: {type: String},
        thumbnail: {type: String},
        settings: {
            boardIds: [String]
        }
    });
    const userModel = mongoose.model('users', userSchema);
    const _selectableFields = 'email firstName lastName thumbnail phone settings';

    const validateByToken = (req, token) => {
        return userModel.findOne({token})
            .select(_selectableFields)
            .then((user) => {
                if (user === null) {
                    return Promise.reject({errmsg: 'Not authorized'});
                }

                delete user.password;

                req.user = user;
                return Promise.resolve(user);
            })
            .catch((error) => {
                return Promise.reject({errmsg: 'Not authorized'});
            });
    };

    const login = (req) => {
        return userModel.findOne({email: req.body.email ? req.body.email : null})
            .select(_selectableFields + ' password')
            .then((user) => {
                if (
                    user === null
                    || typeof req.body.password === 'undefined'
                    || !passwordHash.verify(req.body.password, user.password)
                ) {
                    return Promise.reject({errmsg: 'Invalid email or password'});
                }

                const jwtToken = jwt.sign({
                    id: user._id,
                }, 'some salt', {});

                return new Promise((resolve, reject) => {
                    userModel.update({_id: user._id}, {token: jwtToken}).then((res) => {
                        user.password = undefined;
                        resolve({
                            token: jwtToken,
                            user: user
                        })
                    });
                });
            });
    };

    const register = (req) => {
        const hashedPassword = passwordHash.generate(req.body.password);
        req.body.password = hashedPassword;

        return new userModel(req.body)
            .save()
            .then((result) => {
                if (!result) {
                    return Promise.reject({errmsg: 'Error in registration'});
                }

                return Promise.resolve(result);
            })
            .catch((error) => {
                return Promise.reject({errmsg: 'Error in registration'});
            });
    };

    const getUsers = (req) => {
        return userModel.find({email: new RegExp('^' + req.query.search)})
            .select('email firstName lastName thumbnail settings')
            .then((result) => {
                return Promise.resolve(result);
            })
            .catch((error) => {
                return Promise.reject({errmsg: 'Error in registration'});
            });
    };

    const updateUser = (req) => {
        delete req.body._id;
        delete req.body.verified;

        if (typeof req.body.password !== 'undefined') {
            const hashedPassword = passwordHash.generate(req.body.password);
            req.body.password = hashedPassword;
        }

        return userModel.findOneAndUpdate({_id: req.user._id}, {$set: req.body}, {new: true, returnNewDocument : true})
            .select(_selectableFields)
            .then((res, err) => {
                if (!res) {
                    return Promise.reject('Error in updating profile')
                }
                return Promise.resolve(res);
            })
    };

    module.exports = {
        register,
        login,
        validateByToken,
        getUsers,
        updateUser,

        model: userModel
    };
})();
