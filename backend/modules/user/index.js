(function() {
    'use strict';

    const userModel = require('./model');
    const middlewares = require('../../middlewares');

    const login = (app) => {
        app.post('/login', (req, res, next) => {
            userModel.login(req)
                .then((result) => {
                    res.status(200).json({
                        success: true,
                        token: result.token,
                        user: result.user
                    });
                })
                .catch((error) => {
                    res.status(500).json({
                        msg: error.errmsg
                    });
                });
        });
    };

    const register = (app) => {
        app.post('/register', (req, res, next) => {
            userModel.register(req)
                .then((result) => {
                    res.status(200).json({
                        success: true
                    });
                })
                .catch((error) => {
                    res.status(500).json({
                        msg: error.errmsg
                    });
                });
        });
    };

    const getUsers = (app) => {
        app.get('/users', middlewares.tokenMiddleware, (req, res, next) => {
            if (typeof req.query.search === 'undefined') {
                req.query.search = '';
            }
            userModel.getUsers(req)
                .then((result) => {
                    res.status(200).json(result);
                })
                .catch((error) => {
                    res.status(500).json({
                        msg: error.errmsg
                    });
                })
        });
    };

    const getMe = (app) => {
        app.get('/me', middlewares.tokenMiddleware, (req, res, next) => {
            req.user.password = undefined;
            res.status(200).json(req.user);
        });
    };

    const updateProfile = (app) => {
        app.put('/me', middlewares.tokenMiddleware, (req, res, next) => {
            userModel.updateUser(req)
                .then((result) => {
                    res.status(200).json(result);
                })
                .catch((error) => {
                    res.status(500).json({
                        msg: typeof error.errmsg !== 'undefined' ? error.errmsg : error
                    });
                })
        });
    };

    module.exports = (app) => {
        login(app);
        register(app);
        getUsers(app);
        getMe(app);
        updateProfile(app);
    }
})();
