(function() {
    'use strict';

    const _ = require('lodash');

    const middlewares = require('../../middlewares');
    const boardModel = require('../board/model');
    const wsServer = require('../../websocket.server');

    const sendError = (res, message) => {
        res.status(500).json({
            msg: message
        });
    };

    const getTickets = (app) => {
        app.get('/boards/:id/columns/:column_id/tickets', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            const column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id);

            if (typeof column === 'undefined') {
                sendError(res, 'Column id is undefined');
                return;
            }

            res.status(200).json(column.tickets);
        });
    };

    const getTicket = (app) => {
        app.get('/boards/:id/columns/:column_id/tickets/:ticket_id', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            const column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());

            if (typeof column === 'undefined') {
                sendError(res, 'Column id is undefined');
                return;
            }

            const ticket = _.find(column.tickets, (item) => item._id.toString() === req.params.ticket_id.toString());

            if (typeof ticket === 'undefined') {
                sendError(res, 'Ticket id is undefined');
                return;
            }

            res.status(200).json(ticket);
        });
    };

    const createTicket = (app) => {
        app.post('/boards/:id/columns/:column_id/tickets', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            const column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id);

            if (typeof column === 'undefined' || typeof req.body.title === 'undefined') {
                sendError(res, 'Can not create ticket');
                return;
            }

            column.tickets.push({
                title: req.body.title,
                description: (typeof req.body.description !== 'undefined') ? req.body.description : '',
                creator: req.user._id
            });
            req.board.save()
                .then((error, result) => {
                    res.status(200).json(column.tickets);
                })
                .catch((error) => {
                    sendError(res, error.errmsg ? error.errmsg : 'Error in creating column');
                })
        });
    };

    const updateTicket = (app) => {
        app.put('/boards/:id/columns/:column_id/tickets/:ticket_id', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            let column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());

            if (typeof column === 'undefined') {
                sendError(res, 'Column id is undefined');
                return;
            }

            const ticket = _.find(column.tickets, (item) => item._id.toString() === req.params.ticket_id.toString());

            if (typeof ticket === 'undefined') {
                sendError(res, 'Ticket id is undefined');
                return;
            }

            if (typeof req.body.title === 'undefined') {
                sendError(res, 'Can not update ticket');
                return;
            }

            if (typeof req.body.title !== 'undefined') {
                ticket.title = req.body.title;
            }
            if (typeof req.body.description !== 'undefined') {
                ticket.description = req.body.description;
            }
            if (typeof req.body.column_id !== 'undefined' && req.body.column_id.toString() !== req.params.column_id.toString()) {
                const newColumn = _.find(req.board.columns, (item) => item._id.toString() === req.body.column_id.toString());

                if (typeof newColumn !== 'undefined') {
                    column.tickets.splice(_.findIndex(column.tickets, (item) => item._id.toString() === req.params.ticket_id.toString()), 1);
                    newColumn.tickets.push(ticket);
                }
            }
            if (typeof req.body.position !== 'undefined') {
                if (typeof req.body.column_id !== 'undefined') {
                    column = _.find(req.board.columns, (item) => item._id.toString() === req.body.column_id.toString());
                }
                const currentPosition = _.findIndex(column.tickets, (item) => item._id.toString() === req.params.ticket_id.toString());
                let newPosition = req.body.position;
                if (newPosition > column.tickets.length) {
                    newPosition = column.tickets.length;
                }

                column.tickets.splice(currentPosition, 1);
                column.tickets.splice(newPosition, 0, ticket);
            }

            req.board.save()
                .then((error, result) => {
                    res.status(200).json({
                        success: true,
                        msg: 'Ticket has updated'
                    });
                })
                .catch((error) => {
                console.log(error);
                    sendError(res, error.errmsg ? error.errmsg : 'Error in updating ticket');
                })
        });
    };

    const deleteTicket = (app) => {
        app.delete('/boards/:id/columns/:column_id/tickets/:ticket_id', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            const column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());

            if (typeof column === 'undefined') {
                sendError(res, 'Column id is undefined');
                return;
            }

            const ticketIndex = _.findIndex(column.tickets, (item) => item._id.toString() === req.params.ticket_id.toString());
            const ticket = _.find(column.tickets, (item) => item._id.toString() === req.params.ticket_id.toString());
            if (typeof ticketIndex === -1) {
                sendError(res, 'Can not delete ticket');
                return;
            }
            column.tickets.splice(ticketIndex, 1);
            req.board.save()
                .then((error, result) => {
                    ticket.assigners.forEach((assigner) => {
                        wsServer.broadcast({
                            type: wsServer.USER_UNASSIGNED_FROM_TICKET,
                            id: assigner._id.toString()
                        });
                    });
                    res.status(200).json({
                        success: true,
                        msg: 'Ticket has deleted'
                    });
                })
                .catch((error) => {
                    sendError(res, error.errmsg ? error.errmsg : 'Error in deleting ticket');
                })
        });
    };

    const assignUser = (app) => {
        app.put('/boards/:id/tickets/:ticket_id/assignUser',
            middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware,
            (req, res, next) => {
                const ticket = _.chain(req.board.columns)
                    .flatMap('tickets')
                    .find((item) => {
                        return item._id.toString() === req.params.ticket_id.toString();
                    })
                    .value();

                if (typeof ticket === 'undefined') {
                    sendError(res, 'Ticket id is undefined');
                    return;
                }

                if (typeof req.body.user_id === 'undefined') {
                    sendError(res, 'User id is undefined');
                    return;
                }

                if (_.findIndex(ticket.assigners, (item) => item._id.toString() === req.body.user_id) === -1) {
                    ticket.assigners.push(req.body.user_id);
                }

                req.board.save()
                    .then((error, result) => {
                        wsServer.broadcast({
                            type: wsServer.USER_ASSIGNED_TO_TICKET,
                            id: req.body.user_id
                        });
                        res.status(200).json({
                            success: true,
                            msg: 'User has assigned'
                        });
                    })
                    .catch((error) => {
                        sendError(res, error.errmsg ? error.errmsg : 'Error in assigning user');
                    })
            }
        );
    };


    const unassignUser = (app) => {
        app.put('/boards/:id/tickets/:ticket_id/unassignUser',
            middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware,
            (req, res, next) => {
                const ticket = _.chain(req.board.columns)
                    .flatMap('tickets')
                    .find((item) => {
                        return item._id.toString() === req.params.ticket_id.toString();
                    })
                    .value();

                if (typeof ticket === 'undefined') {
                    sendError(res, 'Ticket id is undefined');
                    return;
                }

                if (typeof req.body.user_id === 'undefined') {
                    sendError(res, 'User id is undefined');
                    return;
                }

                const index = _.findIndex(ticket.assigners, (item) => item._id.toString() === req.body.user_id.toString());
                if (index === -1) {
                    sendError(res, 'User not found');
                    return;
                }

                ticket.assigners.splice(index, 1);
                req.board.save()
                    .then((error, result) => {
                        wsServer.broadcast({
                            type: wsServer.USER_UNASSIGNED_FROM_TICKET,
                            id: req.body.user_id
                        });
                        res.status(200).json({
                            success: true,
                            msg: 'User has unassigned'
                        });
                    })
                    .catch((error) => {
                        sendError(res, error.errmsg ? error.errmsg : 'Error in removing user fromassigners');
                    })
            }
        );
    };

    module.exports = (app) => {
        getTickets(app);
        getTicket(app);
        createTicket(app);
        updateTicket(app);
        deleteTicket(app);
        assignUser(app);
        unassignUser(app);
    }
})();