(function() {
    'use strict';

    const _ = require('lodash');

    const middlewares = require('../../middlewares');
    const boardModel = require('../board/model');
    const wsServer = require('../../websocket.server');

    const sendError = (res, message) => {
        res.status(500).json({
            msg: message
        });
    };

    const getColumns = (app) => {
        app.get('/boards/:id/columns', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            res.status(200).json(req.board.columns)
        });
    };

    const getColumn = (app) => {
        app.get('/boards/:id/columns/:column_id', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            const column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());

            if (typeof column === 'undefined') {
                sendError(res, 'Column id is undefined');
                return;
            }

            res.status(200).json(column);
        });
    };

    const createColumn = (app) => {
        app.post('/boards/:id/columns', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            if (typeof req.body.name === 'undefined') {
                sendError(res, 'Can not create column');
                return;
            }

            req.board.columns.push({
                name: req.body.name
            });

            req.board.save()
                .then((error, result) => {
                    res.status(200).json(req.board.columns);
                })
                .catch((error) => {
                    sendError(res, error.errmsg ? error.errmsg : 'Error in creating column');
                })
        });
    };

    const updateColumn = (app) => {
        app.put('/boards/:id/columns/:column_id', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            // if (typeof req.body.name === 'undefined') {
            //     sendError(res, 'Can not update column');
            //     return;
            // }

            const column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());

            if (typeof column === 'undefined') {
                sendError(res, 'Column id is undefined');
                return;
            }

            if (typeof req.body.name !== 'undefined') {
                column.name = req.body.name;
            }
            if (typeof req.body.position !== 'undefined') {
                const currentPosition = _.findIndex(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());
                const newPosition = req.body.position;

                req.board.columns.splice(currentPosition, 1);
                req.board.columns.splice(newPosition, 0, column);
            }

            req.board.save()
                .then((error, result) => {
                    res.status(200).json({
                        success: true,
                        msg: 'Column has updated'
                    });
                })
                .catch((error) => {
                    sendError(res, error.errmsg ? error.errmsg : 'Error in updating column');
                });
        });
    };

    const deleteColumn = (app) => {
        app.delete('/boards/:id/columns/:column_id', middlewares.tokenMiddleware, middlewares.boardAvailableMiddleware, (req, res, next) => {
            const index = _.findIndex(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());
            const column = _.find(req.board.columns, (item) => item._id.toString() === req.params.column_id.toString());

            if (index === -1) {
                sendError(res, 'Column not found');
                return;
            }
            const tickets = column.tickets;

            req.board.columns.splice(index, 1);
            req.board.save()
                .then((error, result) => {
                    tickets.forEach((ticket) => {
                        ticket.assigners.forEach((assigner) => {
                            wsServer.broadcast({
                                type: wsServer.USER_UNASSIGNED_FROM_TICKET,
                                id: assigner._id.toString()
                            });
                        })
                    });

                    res.status(200).json({
                        success: true,
                        msg: 'Column has deleted'
                    });
                })
                .catch((error) => {
                    sendError(res, error.errmsg ? error.errmsg : 'Error in deleting column');
                });
        });
    };

    module.exports = (app) => {
        getColumns(app);
        getColumn(app);
        createColumn(app);
        updateColumn(app);
        deleteColumn(app);
    }
})();