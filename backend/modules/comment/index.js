(function() {
    const middlewares = require('../../middlewares');
    const boardModel = require('../board/model');

    const getComments = (app) => {
        app.get('/boards/:id/columns/:column_id/tickets/:ticket_id/comments', middlewares.tokenMiddleware, (req, res, next) => {
            res.send('Test boards');
        });
    };

    const getComment = (app) => {
        app.get('/boards/:id/columns/:column_id/tickets/:ticket_id/comments/:comment_id', middlewares.tokenMiddleware, (req, res, next) => {
            res.send('Test get board');
        });
    };

    const createComment = (app) => {
        app.post('/boards/:id/columns/:column_id/tickets/:ticket_id/comments/', middlewares.tokenMiddleware, (req, res, next) => {
            res.send('Test create board');
        });
    };

    const updateComment = (app) => {
        app.post('/boards/:id/columns/:column_id/tickets/:ticket_id/comments/:comment_id', middlewares.tokenMiddleware, (req, res, next) => {
            res.send('Test update board');
        });
    };

    const deleteComment = (app) => {
        app.post('/boards/:id/columns/:column_id/tickets/:ticket_id/comments/delete/:comment_id', middlewares.tokenMiddleware, (req, res, next) => {
            res.send('Test delete board');
        });
    };

    module.exports = (app) => {
        getComments(app);
        getComment(app);
        createComment(app);
        updateComment(app);
        deleteComment(app);
    }
})();