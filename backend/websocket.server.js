const server = require('http').createServer();
const io = require('socket.io')(server);

io.on('connection', function(client){
    client.on('event', function(data){});
    client.on('disconnect', function(){});
});
server.listen(3001);

const broadcast = (data) => {
    io.emit(data.type, data);
};

const USER_ASSIGNED_TO_TICKET = 'USER_ASSIGNED_TO_TICKET';
const USER_UNASSIGNED_FROM_TICKET = 'USER_UNASSIGNED_FROM_TICKET';
const USER_ASSIGNED_TO_BOARD = 'USER_ASSIGNED_TO_BOARD';
const USER_UNASSIGNED_FROM_BOARD = 'USER_UNASSIGNED_FROM_BOARD';

module.exports = {
    broadcast,

    USER_ASSIGNED_TO_TICKET,
    USER_UNASSIGNED_FROM_TICKET,
    USER_ASSIGNED_TO_BOARD,
    USER_UNASSIGNED_FROM_BOARD
};