/**
 * Created by developer on 25.05.17.
 */
(function() {
    'use strict';

    /**
     * method
     * @param {{ [key: string]: Promise<any[]> }} inputObj object with promises in values
     * @return {Promise<{ [key: string]: any[] }>} promise with object with received arrays
     */
    function promiseAllToObject (inputObj) {
        function mapObjectToArray(obj, cb) {
            const res = [];
            for (let key in obj) {
                res.push(cb(obj[key], key));
            }
            return res;
        }

        return Promise.all(
            mapObjectToArray(inputObj, (arg, key) => {
                return arg
                    .then(res => {
                        return {key: key, res: res};
                    });
            })
        )
            .then(arr => {
                const obj = {};
                for (let i = 0; i < arr.length; i++) {
                    obj[arr[i].key] = arr[i].res;
                }
                return obj;
            })
            .catch(err => console.log('Promise.all ERROR: ', err));
    }

    module.exports = {
        promiseAllToObject
    };
})();
