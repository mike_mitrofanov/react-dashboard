export default [
    {
        "_id": "5919c3fec92b424a423a60f1",
        "name": "Board 1",
        "owner": {
            "_id": "5911f8df670d4f55e58144fe",
            "email": "email@example.com"
        },
        "columns": []
    },
    {
        "_id": "591c4413e54d8a221349591a",
        "name": "Board 2",
        "owner": {
            "_id": "5911f8df670d4f55e58144fe",
            "email": "email@example.com"
        },
        "columns": []
    }
];
