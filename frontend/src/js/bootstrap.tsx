import * as React from 'react';
import { render } from 'react-dom';
import 'reflect-metadata';
import 'jquery';
import 'bootstrap';
import AppComponent from './components/App';

render(<AppComponent/>, document.getElementById('content'));
