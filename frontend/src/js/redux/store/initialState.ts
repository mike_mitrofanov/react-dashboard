import { IAppSession } from '../reducers/authReducer';
import { ISchemaEntities } from './schema/ISchemaEntities';

export interface IAppState {
  session: IAppSession;
  entities: ISchemaEntities;
}

export const InitialState: IAppState = {
  session: {
    user: null,
    loading: false,
  },
  entities: {
    normalized: {
      boards: {},
      columns: {},
      tickets: {},
      users: {},
    },
    ids: {
      boards: [],
      columns: [],
      tickets: [],
      users: [],
    },
  },
};
