export interface ISchemaEntities {
    normalized: {
        boards: object,
        columns: object,
        tickets: object,
        users: object,
    };
    ids: {
        boards: string[],
        columns: string[],
        tickets: string[],
        users: string[],
    };
}
