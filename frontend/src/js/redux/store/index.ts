import { injectable } from 'inversify';
declare let window: Window & { devToolsExtension: any, __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any };

import { createStore, applyMiddleware, compose, Store } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import RootReducer from '../reducers';

@injectable()
export default class AppStore {

    public history: any;
    public store: Store<any>;

    constructor() {
        this.history = createHistory();
        this.createReduxStore();
    }

    private createReduxStore(): void {
        const composeEnhancers: any = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        this.store = composeEnhancers(
            applyMiddleware(thunkMiddleware),
            applyMiddleware(routerMiddleware(this.history))
        )(createStore)(RootReducer);
    }

}
