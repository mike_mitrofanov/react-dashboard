import { AuthDecorator } from 'redux-auth-wrapper';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import IRouteDefinition from './IRouteDefinition';

export const applyMiddlewares2Component: any = (
    component: React.ComponentClass<RouteComponentProps<any> | undefined>,
    middlewares: AuthDecorator<any>[]
): React.ComponentClass<RouteComponentProps<any> | undefined> => {

    if (middlewares && middlewares.length > 0) {
        middlewares.forEach((middleware) => {
            component = middleware(component);
        });
    }

    return component;
};

export default {
    applyMiddlewares2Component: applyMiddlewares2Component,
    extractRouteParams: (def: IRouteDefinition) => {
        let {component, path, exact, middlewares, children} = def;
        return {
            render: (props: RouteComponentProps<any>) => {
                props.match.params.childRoutes = children;
                let wrapped: React.ComponentClass<RouteComponentProps<any> | undefined> = applyMiddlewares2Component(
                    component,
                    middlewares
                );
                return React.createElement(wrapped, {
                    ...props,
                });
            },
            path,
            exact,
        };
    },
};
