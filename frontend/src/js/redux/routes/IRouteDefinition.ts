import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { AuthDecorator } from 'redux-auth-wrapper';

interface IRouteDefinition {
    component: React.ComponentClass<RouteComponentProps<any> | undefined>;
    path?: string;
    exact?: boolean;
    middlewares?: AuthDecorator<any>[];
    children?: IRouteDefinition[];
}

export default IRouteDefinition;
