/**
 * Created by Andrew on 11.05.2017.
 */

import { UserAuthWrapper, AuthWrapperConfig } from 'redux-auth-wrapper';
import { push } from 'react-router-redux';
import RouteNames from './RouteNames';
import Preloader from '../../components/Navigation/Preloader';
import { IAppState } from '../store/initialState';

const userAuthConfig: AuthWrapperConfig<any, any, any> = {
    authSelector: (state: IAppState, ownProps) => {
        return state.session.user;
    },
    failureRedirectPath: '/',
    authenticatingSelector: (state: IAppState, ownProps) => {
        return state.session.loading;
    },
    LoadingComponent: Preloader,
    redirectAction: args => {
        return push(RouteNames.index);
    },
    predicate: user => {
        return !!(user && user._id);
    },
    wrapperDisplayName: 'UserIsAuthenticated',
};

const userNoAuthConfig: AuthWrapperConfig<any, any, any> = {
    authSelector: (state: IAppState, ownProps) => {
        return state.session.user;
    },

    failureRedirectPath: '/',

    authenticatingSelector: (state: IAppState, ownProps) => {
        return state.session.loading;
    },

    LoadingComponent: Preloader,

    redirectAction: args => {
        return push(RouteNames.index);
    },

    predicate: user => {
        return !user || !user._id;
    },

    wrapperDisplayName: 'UserIsAuthenticated',
};

export default {
    authMiddleware: UserAuthWrapper(userAuthConfig),
    noAuthMiddleware: UserAuthWrapper(userNoAuthConfig),
};
