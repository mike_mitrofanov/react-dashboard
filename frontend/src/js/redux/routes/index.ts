import IRouteDefinition from './IRouteDefinition';
import Boards from '../../components/Boards/Boards';
import Dashboard from '../../components/Dashboard/Dashboard';
import Landing from '../../components/Landing/Landing';
import Auth from '../../components/Auth/Auth';

import RouteNames from './RouteNames';
import Middlewares from './Middlewares';
import HeaderFooterRouteWrapper from '../../components/Layouts/HeaderFooterRouteWrapper';
import HeaderRouteWrapper from '../../components/Layouts/HeaderRouteWrapper';

export const RouteDefinitions: IRouteDefinition[] = [
    {
        component: Landing,
        path: RouteNames.index,
        exact: true,
    },
    {
        path: RouteNames.auth,
        component: HeaderFooterRouteWrapper,
        children: [
            {
                component: Auth,
                middlewares: [Middlewares.noAuthMiddleware],
            },
        ],
    },
    {
        component: HeaderRouteWrapper,
        path: RouteNames.boards,
        middlewares: [Middlewares.authMiddleware],
        children: [
            {
                component: Boards,
                exact: true,
                path: RouteNames.boards,
            },
            {
                component: Dashboard,
                path: RouteNames.dashboard,
            },
            {
                component: Dashboard,
                path: RouteNames.ticket,
            },
        ],
    },
];
