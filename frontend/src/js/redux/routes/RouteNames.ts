/**
 * Created by Andrew on 11.05.2017.
 */

export default {
    index: '/',
    dashboard: '/boards/:boardId',
    boards: '/boards',
    auth: '/auth',
    ticket: '/boards/:boardId/columns/:columnId/tickets/:ticketId',
};
