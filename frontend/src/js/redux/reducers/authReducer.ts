import { Reducer } from 'redux';
import { IUser } from '../../common/models/IUser';
import { IAction } from '../../common/IAction';
import { SET_SESSION_USER, SET_USER_LOADING } from '../../common/actionNames';
import { InitialState } from '../store/initialState';

export interface IAppSession {
    user: IUser;
    loading: boolean;
}

const authReducer: Reducer<IAppSession> = (state = InitialState.session, action: IAction<any>): IAppSession => {
    switch (action.type) {
        case SET_SESSION_USER:
            return Object.assign({}, state, {user: action.payload});

        case SET_USER_LOADING:
            return Object.assign({}, state, {loading: action.payload});

        default:
            return state;
    }
};

export default authReducer;
