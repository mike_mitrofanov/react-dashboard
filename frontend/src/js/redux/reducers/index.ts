import { combineReducers } from 'redux';
import session from './authReducer';
import entities from './entity';

export default combineReducers({
    session,
    entities,
});
