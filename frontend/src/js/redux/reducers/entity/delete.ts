import { ISchemaEntities } from '../../store/schema/ISchemaEntities';
import { IEntityDeleteAction } from '../../../common/schema/IEntityDeleteAction';
import * as _ from 'lodash';
import { CascadeSchemaDependencies } from '../../../common/schema/DataSchema';
import { IEntity } from '../../../common/schema/IEntity';

function removeEntityFromParent(state: ISchemaEntities, entityIds: string[], def: any, scope: any) {
    let keys2discover: string[] = scope || _.keys(state.normalized[def.entityType]);

    keys2discover.forEach(key => {
        if (def.type === 'array') {
            if (state.normalized[def.entityType][key]) {
                state.normalized[def.entityType][key][def.field] = _.difference(
                    state.normalized[def.entityType][key][def.field], entityIds
                );
            }
        } else {
            if (state.normalized[def.entityType][key] &&
                entityIds.indexOf(state.normalized[def.entityType][key][def.field]) >= 0) {
                state.normalized[def.entityType][key][def.field] = null;
            }
        }
    });

    return state;
}

function buildDependencyTree(state: ISchemaEntities, entityIds: string[], entityName: string) {
    function getChildren2delete(entity: IEntity, etype: string, items2remove: {}) {
        if (!items2remove[etype]) {
            items2remove[etype] = [];
        }
        items2remove[etype].push(entity._id);

        if (CascadeSchemaDependencies[etype] && CascadeSchemaDependencies[etype].children) {
            CascadeSchemaDependencies[etype].children.forEach((children) => {
                if (children.type === 'array') {
                    entity[children.field].forEach(id => {
                        let childrenEntity: IEntity = state.normalized[children.entityType][id];
                        items2remove = getChildren2delete(childrenEntity, children.entityType, items2remove);
                    });
                } else {
                    if (!items2remove[children.entityType]) {
                        items2remove[children.entityType] = [];
                    }
                    items2remove[children.entityType].push(entity[children.field]);
                }
            });
        }

        return items2remove;
    }

    let items2remove: any = {};

    entityIds.forEach((entityId) => {
        let entity: IEntity = state.normalized[entityName][entityId];
        items2remove = getChildren2delete(entity, entityName, items2remove);
    });

    return items2remove;
}

export function deleteEntities(state: ISchemaEntities, action: IEntityDeleteAction): ISchemaEntities {
    let newState: ISchemaEntities = Object.assign({}, state);
    let items2remove: any = buildDependencyTree(newState, action.payload.ids, action.payload.entityName);

    // remove entities and them children from store
    _.forOwn(items2remove, (value: any[], key) => {
        if (!newState.normalized[key]) {
            throw 'Unknown entity name';
        }

        value.forEach(id => {
            delete newState.normalized[key][id];
            newState.ids[key] = newState.ids[key].filter(i => i !== id);
        });
    });

    // unlink self from parents
    let dependencies: any = CascadeSchemaDependencies[action.payload.entityName];
    let parentScope: any = action.payload.parentScope;
    if (dependencies && dependencies.parents) {
        dependencies.parents.forEach(
            (parentDef) => {
                const parentScopeType: string = parentScope[parentDef.entityType];
                let scope: string | null = parentScope && parentScopeType ? parentScopeType : null;
                newState = removeEntityFromParent(state, action.payload.ids, parentDef, scope);
            }
        );
    }

    return {...newState};
}
