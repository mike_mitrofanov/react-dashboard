import { Reducer } from 'redux';
import { ENTITY_UPDATE, ENTITY_DELETE, SWAP_BOARDS } from '../../../common/actionNames';
import { InitialState } from '../../store/initialState';
import { ISchemaEntities } from '../../store/schema/ISchemaEntities';
import { IEntityAction } from '../../../common/schema/IEntityAction';
import { IEntityDeleteAction } from '../../../common/schema/IEntityDeleteAction';
import { updateEntities } from './update';
import { deleteEntities } from './delete';

const entitiesReducer: Reducer<ISchemaEntities> = (state = InitialState.entities, action: any): ISchemaEntities => {
    switch (action.type) {
        case ENTITY_UPDATE:
            return updateEntities(state, <IEntityAction>action);
        case ENTITY_DELETE:
            return deleteEntities(state, <IEntityDeleteAction>action);
        case SWAP_BOARDS:
            return {
                ...state,
                ids: {
                    ...state.ids,
                    boards: action.payload.data.map(item => item._id),
                },
            };
        default:
            return state;
    }
};

export default entitiesReducer;
