import { ISchemaEntities } from '../../store/schema/ISchemaEntities';
import { IEntityAction } from '../../../common/schema/IEntityAction';
import * as _ from 'lodash';
import { MERGE_STRATEGY } from '../../../common/schema/MergeStrategy';
import { IEntityNode } from '../../../common/schema/IEntityNode';

const StrategyHandlers: {} = {
    [MERGE_STRATEGY.EXTEND]: extendMergeStrategy,
    [MERGE_STRATEGY.REWRITE]: hardMergeStrategy,
};

function extendMergeStrategy(oldEntities: {}, newEntities: {}) {
    const entities: {} = Object.assign({}, oldEntities);

    _.forOwn(newEntities, (entity, id) => {
        if (entities[id]) {
            _.forOwn(entity, (value, field) => {
                if (value instanceof Array && entities[id][field] instanceof Array) {
                    entities[id][field] = _.uniq(_.concat([], entities[id][field], value));
                } else {
                    entities[id][field] = value;
                }
            });
        } else {
            entities[id] = entity;
        }
    });

    return entities;
}

function hardMergeStrategy(oldEntities: {}, newEntities: {}) {
    return Object.assign({}, oldEntities, newEntities);
}

export function updateEntities(state: ISchemaEntities, action: IEntityAction): ISchemaEntities {
    const newState: ISchemaEntities = Object.assign({}, state);
    const updatedData: IEntityNode = action.payload.data;
    const mergeStrategy: any = StrategyHandlers[action.payload.mergeStrategy || MERGE_STRATEGY.REWRITE];

    _.forOwn(updatedData.entities, (entities, key) => {
        newState.normalized[key] = mergeStrategy(state.normalized[key], entities);
        newState.ids[key] = _.uniq(newState.ids[key].concat(_.keys(entities)));
    });

    return newState;
}
