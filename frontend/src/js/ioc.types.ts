export const TYPES: any = {
    AppStore: 'AppStore',
    AuthService: 'AuthService',
    AppComponent: 'AppComponent',
    DispatchProvider: 'DispatchProvider',
    HttpClient: 'HttpClient',
    SocketClient: 'SocketClient',
    BoardsService: 'BoardsService',
    ColumnService: 'ColumnService',
};
