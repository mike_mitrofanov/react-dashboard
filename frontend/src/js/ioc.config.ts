import { Container } from 'inversify';
import { AuthService } from './services/app/Auth/AuthService';
import AppStore from './redux/store/index';
import { DispatchProvider } from './services/utils/DispatchProvider';
import { TYPES } from './ioc.types';
import { Http } from './services/utils/http';
import { Socket } from './services/utils/socket';
import { BoardsService } from './services/app/Board/BoardsService';
import { ColumnService } from './services/app/Column/ColumnService';
import { IDispatchProvider } from './services/base/IDispatchProvider'; /* tslint:disable-line */

let iocContainer: Container = new Container();

iocContainer.bind<AppStore>(TYPES.AppStore).to(AppStore).inSingletonScope();
iocContainer.bind<Http>(TYPES.HttpClient).to(Http).inSingletonScope();
iocContainer.bind<Socket>(TYPES.SocketClient).to(Socket).inSingletonScope();
iocContainer.bind<IDispatchProvider>(TYPES.DispatchProvider).to(DispatchProvider).inSingletonScope();
iocContainer.bind<AuthService>(TYPES.AuthService).to(AuthService).inSingletonScope();
iocContainer.bind<BoardsService>(TYPES.BoardsService).to(BoardsService).inSingletonScope();
iocContainer.bind<ColumnService>(TYPES.ColumnService).to(ColumnService).inSingletonScope();

export { iocContainer };
