import { AxiosPromise, AxiosResponse } from 'axios';
import { BaseRestService } from '../../base/BaseRestService';
import { IColumn } from '../../../common/models/IColumn';
import { ColumnSchema } from '../../../common/schema/DataSchema';
import { ITicket } from '../../../common/models/ITicket';
import { MERGE_STRATEGY } from '../../../common/schema/MergeStrategy';

export class ColumnService extends BaseRestService<IColumn> {

    constructor() {
        super();
        this.entitySchema = ColumnSchema;
        this.entityName = 'columns';
        this.baseUrl = '/columns';
    }

    public removeColumn(columnId: string, boardId: string): AxiosPromise {
        return this._httpClient.del(`boards/${boardId}/columns/${columnId}`)
            .then((res: AxiosResponse): AxiosResponse => {
                this.deleteEntity(columnId, {boards: [boardId]});
                return res;
            });
    }

    public addTicket(boardId: string, columnId: string, ticket: ITicket): AxiosPromise {
        return this._httpClient.post(`/boards/${boardId}/columns/${columnId}/tickets`, ticket)
            .then((res: AxiosResponse): AxiosResponse => {
                this.updateEntity(<any>{_id: columnId, tickets: res.data}, MERGE_STRATEGY.EXTEND);
                return res;
            });
    }

    public editTicket(boardId: string, columnId: string, ticket: ITicket): AxiosPromise {
        return this._httpClient.put(`/boards/${boardId}/columns/${columnId}/tickets/${ticket._id}`, ticket)
            .then((res: AxiosResponse): AxiosResponse => { // res is useless in put method
                this.updateEntity(<any>{_id: columnId, tickets: [ticket]}, MERGE_STRATEGY.EXTEND);
                return res;
            });
    }
}
