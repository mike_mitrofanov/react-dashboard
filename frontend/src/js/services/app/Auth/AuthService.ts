import { IUser } from '../../../common/models/IUser';
import LocalStorage from '../../utils/LocalStorage';
import { BaseHttpService } from '../../base/BaseHttpService';
import { SET_SESSION_USER, SET_USER_LOADING } from '../../../common/actionNames';
import { AxiosPromise, AxiosResponse, AxiosError } from 'axios';
import { injectable } from 'inversify';

@injectable()
export class AuthService extends BaseHttpService {

    constructor() {
        super();
    }

    public tryAutoLogin(): AxiosPromise {
        const token: string = LocalStorage.get('token');
        this._setUserLoading(true);
        if (token) {
            return this._httpClient.get('/me')
                .then((res: AxiosResponse): AxiosResponse => {
                    const user: IUser = res.data;
                    if (user && Object.keys(user).length > 0) {
                        this._setUser2Session(user);
                    } else {
                        this.logout();
                    }
                    return res;
                })
                .catch((err: AxiosError): AxiosPromise => {
                    this.logout();
                    return Promise.reject(err);
                });
        } else {
            return Promise.reject(this._removeUserFromSession());
        }
    }

    public login(email: string, password: string): AxiosPromise {
        this._setUserLoading(true);
        return this._httpClient
            .post('/login', {email, password})
            .then((res: AxiosResponse): AxiosResponse => {
                const {success, token, user} = res.data;
                if (success) {
                    this._setUser2Session(user, token);
                } else {
                    this._removeUserFromSession();
                }
                return res;
            })
            .catch((e: AxiosError): AxiosPromise => {
                // TODO: handle login error
                this._removeUserFromSession();
                return Promise.reject(e);
            });
    }

    public register(email: string, password: string): AxiosPromise {
        return this._httpClient.post('/register', {email, password});
    }

    public logout(): void {
        LocalStorage.remove('user');
        this._httpClient.removeToken();
        this._removeUserFromSession();
    }

    public editUser(user: IUser, successCallback: (dispatch: (obj: {}) => void) => void): AxiosPromise {
        return this._httpClient
            .put(`/me`, user)
            .then((res: AxiosResponse): AxiosResponse => {
                this._setUser2Session(user);
                setTimeout(() => {
                    successCallback(this.dispatch.bind(this));
                });
                return res;
            });
    }

    private _removeUserFromSession(): void {
        this._setUser2Session(null);
    }

    private _setUser2Session(user: IUser, token?: string): void {
        if (token) {
            LocalStorage.set('user', user);
            this._httpClient.setToken(token);
            this._socketClient.setUser(user);
        }
        this.dispatch({
            type: SET_SESSION_USER,
            payload: user,
        });
        this._setUserLoading(false);
    }

    private _setUserLoading(value: boolean): void {
        this.dispatch({
            type: SET_USER_LOADING,
            payload: value,
        });
    }
}
