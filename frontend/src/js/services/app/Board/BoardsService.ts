import { IBoard } from '../../../common/models/IBoard';
import { BaseRestService } from '../../base/BaseRestService';
import { BoardSchema } from '../../../common/schema/DataSchema';
import { IColumn } from '../../../common/models/IColumn';
import { MERGE_STRATEGY } from '../../../common/schema/MergeStrategy';
import { AxiosPromise, AxiosResponse } from 'axios';

export class BoardsService extends BaseRestService<IBoard> {

    constructor() {
        super();
        this.entitySchema = BoardSchema;
        this.entityName = 'boards';
        this.baseUrl = '/boards';
    }

    public addColumn(boardId: string, column: IColumn): AxiosPromise {
        return this._httpClient
            .post(`${this.baseUrl}/${boardId}/columns`, column)
            .then((res: AxiosResponse): AxiosResponse => {
                this.updateEntity(<any>{_id: boardId, columns: res.data}, MERGE_STRATEGY.EXTEND);
                return res;
            });
    }

    public addBoard(boardName: string): AxiosPromise {
        return this._httpClient
            .post(this.baseUrl, {name: boardName})
            .then((res: AxiosResponse): AxiosResponse => {
                this.updateEntity(res.data);
                return res;
            });
    }

    public editBoard(board: { _id: string, [property: string]: any }): AxiosPromise {
        const boardId: string = board._id;
        let _board: { _id: string, [property: string]: any } = JSON.parse(JSON.stringify(board));
        delete _board._id;

        return this._httpClient
            .put(`${this.baseUrl}/${boardId}`, _board)
            .then((res: AxiosResponse): AxiosResponse => {
                this.updateEntity(board);
                return res;
            });
    }

    public changeColumnOrder(board: { _id: string, columns?: IColumn[], [property: string]: any }): AxiosPromise {
        if (board.columns) { // this condition just check types
            return this._httpClient
                .put(`${this.baseUrl}/${board._id}/columnsOrder`, {columnIds: board.columns.map(column => column._id)})
                .then((res: AxiosResponse): AxiosResponse => {
                    this.updateEntity(board);
                    return res;
                });
        } else {
            return Promise.reject({message: 'ERROR: no columns sent'});
        }
    }

    public removeBoard(id: string): AxiosPromise {
        return this.remove(id);
    }

}
