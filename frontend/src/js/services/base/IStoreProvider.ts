import { Store } from 'redux';
import { IAppState } from '../../redux/store/initialState';

export interface IStoreProvider {
    getStore(): Store<IAppState>;
}
