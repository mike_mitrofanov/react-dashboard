import { DispatchProvider } from '../utils/DispatchProvider';
import { iocContainer } from '../../ioc.config';
import { TYPES } from '../../ioc.types';
import { IAction } from '../../common/IAction';
import { injectable } from 'inversify';
import { Schema } from 'normalizr';
import { IAppState } from '../../redux/store/initialState';
import { Store } from 'redux';

@injectable()
export class BaseReduxService {

    protected _dispatchProvider: DispatchProvider;

    protected schema: Schema;

    constructor() {
        this._dispatchProvider = iocContainer.get<DispatchProvider>(TYPES.DispatchProvider);
    }

    protected dispatch(action: IAction<any>): void {
        this._dispatchProvider.dispatch(action, this.schema);
    }

    protected getStore(): Store<IAppState> {
        return this._dispatchProvider.getStore();
    }

}
