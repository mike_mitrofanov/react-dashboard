export interface IService<T> {
    loadInstance(id: string): void;
    create(instance: T): void;
    update(instance: T): void;
    remove(instance: T | string | number): void;
    loadAll(): void;
}
