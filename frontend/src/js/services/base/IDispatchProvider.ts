import { IAction } from '../../common/IAction';

export interface IDispatchProvider {
    dispatch(action: IAction<any>): void;
}
