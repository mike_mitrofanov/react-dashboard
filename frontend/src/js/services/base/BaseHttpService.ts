import { Http } from '../utils/http';
import { iocContainer } from '../../ioc.config';
import { TYPES } from '../../ioc.types';
import { BaseReduxService } from './BaseReduxService';
import { injectable } from 'inversify';
import { Socket } from '../utils/socket';

@injectable()
export abstract class BaseHttpService extends BaseReduxService {

    protected _httpClient: Http;
    protected _socketClient: Socket;

    constructor() {
        super();
        this._httpClient = iocContainer.get<Http>(TYPES.HttpClient);
        this._socketClient = iocContainer.get<Socket>(TYPES.SocketClient);
    }

}
