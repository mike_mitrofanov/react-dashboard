import { IService } from './IService';
import { BaseHttpService } from './BaseHttpService';
import { injectable } from 'inversify';
import { normalize, Schema, schema } from 'normalizr';
import { AxiosPromise, AxiosResponse, AxiosError } from 'axios';
import { IEntity } from '../../common/schema/IEntity';
import { IEntityDeleteAction } from '../../common/schema/IEntityDeleteAction';
import { ENTITY_DELETE, ENTITY_UPDATE } from '../../common/actionNames';
import { IEntityAction } from '../../common/schema/IEntityAction';
import { IEntityNode } from '../../common/schema/IEntityNode';

@injectable()
export abstract class BaseRestService<T extends IEntity> extends BaseHttpService implements IService<T> {

    protected baseUrl: string;
    protected entityName: string;
    protected entitySchema: Schema;

    constructor() {
        super();
    }

    public loadInstance(id: string): AxiosPromise {
        return this._httpClient
            .get(`${this.baseUrl}/` + id)
            .then((res: AxiosResponse): AxiosResponse => {
                let entity: IEntity = res.data;
                this.updateEntity(entity);
                return res;
            })
            .catch((err: AxiosError): void => {
                throw(err);
            });
    }

    public create(instance: IEntity): void {
        throw new Error('Method not implemented.');
    }

    public update(entity: IEntity): AxiosPromise {
        return this._httpClient
            .put(`${this.baseUrl}/` + entity._id, entity)
            .then((res: AxiosResponse): AxiosResponse => {
                let entityResult: IEntity = res.data;
                this.updateEntity(entityResult);
                return res;
            })
            .catch((err: AxiosError): void => {
                throw(err);
            });
    }

    public remove(instanceId: string): AxiosPromise {
        return this._httpClient
            .del(`${this.baseUrl}/` + instanceId)
            .then((res: AxiosResponse): AxiosResponse => {
                this.deleteEntity(instanceId, null);
                return res;
            })
            .catch((err: AxiosError): void => {
                throw(err);
            });
    }

    public loadAll(): AxiosPromise {
        return this._httpClient
            .get(`${this.baseUrl}/`)
            .then((res: AxiosResponse): AxiosResponse => {
                let entities: IEntity[] = res.data;
                this.updateEntities(entities);
                return res;
            })
            .catch((err: AxiosError): AxiosPromise => {
                console.log(err);
                return Promise.reject(err);
            });
    }

    protected updateEntity(entity: IEntity, strategy?: string) {
        this._updateEntity(entity, this.entitySchema, strategy);
    }

    protected updateEntities(entities: IEntity[], strategy?: string) {
        this._updateEntity(entities, new schema.Array(this.entitySchema), strategy);
    }

    protected deleteEntity(entityId: string, parentScope?: any) {
        this.deleteEntities([entityId], parentScope);
    }

    protected deleteEntities(entityIds: string[], parentScope?: any) {
        let action: IEntityDeleteAction = {
            type: ENTITY_DELETE,
            payload: {
                entityName: this.entityName,
                ids: entityIds,
                parentScope: parentScope,
            },
        };
        this.dispatch(action);
    }

    private _updateEntity(data: any, schema: Schema, strategy?: string) {
        let action: IEntityAction = {
            type: ENTITY_UPDATE,
            payload: {
                data: this.normalizeData(data, schema),
                entityName: this.entityName,
                mergeStrategy: strategy,
            },
        };
        this.dispatch(action);
    }

    private normalizeData(data: any, schema: Schema): IEntityNode {
        const normalized: any = normalize(data, schema);
        return {
            ids: normalized.result,
            entities: normalized.entities,
        };
    }

}
