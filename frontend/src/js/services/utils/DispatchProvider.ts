import { IDispatchProvider } from '../base/IDispatchProvider';
import { injectable } from 'inversify';
import AppStore from '../../redux/store/index';
import { IAction } from '../../common/IAction';
import { TYPES } from '../../ioc.types';
import { Schema } from 'normalizr';
import { iocContainer } from '../../ioc.config';
import { IStoreProvider } from '../base/IStoreProvider';
import { IAppState } from '../../redux/store/initialState';
import { Store } from 'redux';

@injectable()
export class DispatchProvider implements IDispatchProvider, IStoreProvider {

    private _appStore: AppStore = iocContainer.get<AppStore>(TYPES.AppStore);

    public dispatch<T>(action: IAction<T>, schema?: Schema): void {
        this._appStore.store.dispatch(action);
    }

    public getStore(): Store<IAppState> {
        return this._appStore.store;
    }

}
