export default class LocalStorage {

    public static set(name: string, data: any): void {
        localStorage.setItem(name, JSON.stringify(data));
    }

    public static get(name: string): any {
        const itemStr: string = localStorage.getItem(name);
        let item: any;

        try {
            item = JSON.parse(itemStr);
        } catch (e) {
            item = itemStr;
        }

        return item;
    }

    public static remove(name: string): void {
        localStorage.removeItem(name);
    }

}
