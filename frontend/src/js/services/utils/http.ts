import axios, { AxiosInstance, AxiosPromise } from 'axios';
import { API_URL } from '../constants';
import { injectable } from 'inversify';

@injectable()
export class Http {

    private http: AxiosInstance;
    private accessToken: string;

    constructor() {
        this.resetHttp();
        if (localStorage.getItem('token')) {
            this.setToken(localStorage.getItem('token'));
        }
    }

    public setToken(token: string) {
        if (!token) {
            throw new Error('no token specified');
        }

        this.accessToken = token;
        localStorage.setItem('token', token);

        this.http = axios.create({
            baseURL: API_URL,
            headers: {
                'Authorization': `Bearer ${token}`,
            },
        });
    }

    public removeToken() {
        this.accessToken = '';
        localStorage.removeItem('token');
    }

    public get(url: string): AxiosPromise {
        return this.http.get(url);
    }

    public post(url: string, data: any): AxiosPromise {
        return this.http.post(url, {...data});
    }

    public put(url: string, data: any): AxiosPromise {
        return this.http.put(url, {...data});
    }

    public del(url: string): AxiosPromise {
        return this.http.delete(url);
    }

    private resetHttp() {
        this.http = axios.create({
            baseURL: API_URL,
            headers: {},
        });
    }

}
