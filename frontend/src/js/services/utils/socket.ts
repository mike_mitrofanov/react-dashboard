import { injectable } from 'inversify';
import * as io from 'socket.io-client';
import { API_SOCKET_URL } from '../constants';
import {
    socketMessageTypes, USER_ASSIGNED_TO_BOARD, USER_ASSIGNED_TO_TICKET,
    USER_UNASSIGNED_FROM_BOARD, USER_UNASSIGNED_FROM_TICKET,
} from '../../common/actionNames';
import { IUser } from '../../common/models/IUser';
import { BaseReduxService } from '../base/BaseReduxService';

interface IPayload {
    type: string;
    id: string;
}

@injectable()
export class Socket extends BaseReduxService {

    private _socket: SocketIOClient.Emitter;

    private _user: IUser = null;

    constructor() {
        super();
        this._socket = io(API_SOCKET_URL);

        this.init();
    }

    public setUser(user: IUser) {
        this._user = user;
    }

    private init() {
        Object.keys(socketMessageTypes)
              .forEach((type: string) => this._socket.on(type, (payload: IPayload) => {
                  if (this._user === null || this._user._id !== payload.id) {
                      return;
                  }

                  switch (type) {
                      case USER_ASSIGNED_TO_TICKET:
                      case USER_UNASSIGNED_FROM_TICKET:
                          // TODO: reload current user's tickets
                          break;
                      case USER_ASSIGNED_TO_BOARD:
                      case USER_UNASSIGNED_FROM_BOARD:
                          // TODO: reload current user's boards
                          break;
                      default:
                          break;
                  }
              }));
    }

}
