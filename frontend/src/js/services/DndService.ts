/**
 * Created by developer on 23.05.17.
 */
import * as React from 'react';

interface IDropTarget<P> {
    hover?: (props: P, monitor?: any, component?: React.Component<P, any>) => void;
}

interface IDndProps {
    index?: number;
    id?: string;
    moveCard?: any;
    [propName: string]: any;
}

interface IDndState {
    [propName: string]: any;
}

export default class DndService {
    public static dropTarget (): IDropTarget<IDndProps> {
        return {
            hover(props: IDndProps, monitor: any, component: React.Component<IDndProps, IDndState>): void {
                const dragIndex: number = monitor.getItem().index;
                const hoverIndex: number = props.index;
                if (dragIndex === hoverIndex) {
                    return;
                }
                props.moveCard(dragIndex, hoverIndex);
                monitor.getItem().index = hoverIndex;
            },
        };
    }

    public static dragSource () {
        return {
            beginDrag (props: IDndProps) {
                return {
                    id: props.id,
                    index: props.index,
                };
            },
        };
    }

    public static moveCard<T> (
        stateArrName: string,
        state: any,
        cb: (obj: any) => void,
        dragIndex: number,
        hoverIndex: number
    ): void {
        const items: T[] = state[stateArrName];
        const dragItem: T = items[dragIndex];
        items.splice(dragIndex, 1);
        items.splice(hoverIndex, 0, dragItem);
        const newState: {} = {};
        newState[stateArrName] = items;
        cb(newState);
    }
}
