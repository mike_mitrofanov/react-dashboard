import * as React from 'react';

const IntroHeader: () => React.ReactElement<any> = () => {
    return (
        <div className='intro-header'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='intro-message'>
                            <h1>Landing Page</h1>
                            <h3>React Dashboard</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default IntroHeader;
