import * as React from 'react';

import IntroHeader from './IntroHeader/IntroHeading';
import Section from './Section/Section';
import IntroFooter from './IntroFooter/IntroFooter';
import Footer from '../Footer/Footer';
import { Header } from '../Header/Header';

interface ISection {
    className: string;
    imgSrc: string;
}

class LandingPage extends React.Component<any, any> {

    public render() {
        const sections: ISection[] = [
            {
                className: 'content-section-a',
                imgSrc: './src/assets/ipad.png',
            },
            {
                className: 'content-section-b',
                imgSrc: './src/assets/dog.png',
            },
            {
                className: 'content-section-a',
                imgSrc: './src/assets/phones.png',
            },
        ];

        return (
            <div>
                <Header/>
                <IntroHeader/>
                {sections.map((section: ISection, index: number) => (
                    <Section key={index} classNameProp={section.className} imgSrc={section.imgSrc}/>
                ))}
                <IntroFooter/>
                <Footer/>
            </div>
        );
    }

}

export default LandingPage;
