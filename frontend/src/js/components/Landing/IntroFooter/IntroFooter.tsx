import * as React from 'react';

const IntroFooter: () => React.ReactElement<any> = () => {
    return (
        <div className='banner'>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-6'>
                        <h2>React dashboard</h2>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default IntroFooter;
