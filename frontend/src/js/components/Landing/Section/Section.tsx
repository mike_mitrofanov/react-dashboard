import * as React from 'react';

interface ILandingSectionProps {
    classNameProp: string;
    imgSrc: string;
}

type ISection = (props: ILandingSectionProps) => React.ReactElement<any>;

const Section: ISection = (props) => {
    return (
        <div className={props.classNameProp}>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-5 col-sm-6'>
                        <hr className='section-heading-spacer'/>
                        <div className='clearfix'/>
                        <h2 className='section-heading'>Lorem ipsum:<br/>Lorem ipsum</h2>
                        <p className='lead'>Lorem ipsum</p>
                    </div>
                    <div className='col-lg-5 col-lg-offset-2 col-sm-6'>
                        <img className='img-responsive' src={props.imgSrc}/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Section;
