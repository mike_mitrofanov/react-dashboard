import * as React from 'react';

interface ITextInputProps {
    type: string;
    name: string;
    value?: string;
    id?: string;
    placeholder?: string;
    onChange?: (value: string) => any;
    onKeyDown?: (keyCode: number) => any;
    onBlur?: (value: string) => void;
}

type ITextInput = (object: ITextInputProps) => React.ReactElement<ITextInputProps>;

const TextInput: ITextInput = ({type, onChange, name, value = null, id = null, placeholder = null, onKeyDown, onBlur}) => {
    const attrs: {} = {
        type,
        id,
        value,
        placeholder,
        onChange: onChange ? e => onChange(e.target.value) : null,
        onKeyDown: onKeyDown ? (e) => onKeyDown(e.keyCode) : null,
        onBlur: onBlur ? e => onBlur(e.target.value) : null,
    };

    return (
        <input className='form-control' {...attrs}/>
    );
};

export default TextInput;
