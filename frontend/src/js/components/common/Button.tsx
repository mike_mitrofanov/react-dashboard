import * as React from 'react';

interface IButtonProps {
    onClick: () => any;
    type?: string;
    className?: string;
    bootstrapBtn?: string;
    children?: any;
    style?: object;
    position?: string;
}

type IButton = (object: IButtonProps) => React.ReactElement<any>;

const Button: IButton = ({type = 'button', onClick, className = 'btn btn-default', bootstrapBtn, children, style, position}) => {
    let disabled: boolean = true;
    if (bootstrapBtn) {
        className = `btn btn-${bootstrapBtn} ${position}`;
        disabled = false;
    } else {
        className = `btn btn-default ${position}`;
    }

    return (
        <button type={type} onClick={onClick} className={className} style={style ? {...style} : null} disabled={disabled}>
            {children}
        </button>
    );
};

export default Button;
