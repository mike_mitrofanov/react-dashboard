import * as React from 'react';

interface IRemoveIconProps {
    onClick: (e: any) => any;
    // showIcon: boolean;
}

type IRemoveIcon = (object: IRemoveIconProps) => React.ReactElement<IRemoveIconProps>;

const RemoveIcon: IRemoveIcon = ({onClick}) => {
    return (
        <div className='glyphicon glyphicon-remove remove-icon' onClick={onClick}/>
    );
};

export default RemoveIcon;
