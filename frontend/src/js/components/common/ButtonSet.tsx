import * as React from 'react';

interface IButtonSetProps {
    children: any[];
    mode?: 'horizontal' | 'vertical';
    margin?: string;
}

type IButtonSet = (object: IButtonSetProps) => React.ReactElement<IButtonSetProps>;

const ButtonSet: IButtonSet = ({children, mode = 'horizontal', margin = '10px'}) => {
    const marginType: string = (mode === 'vertical') ? 'marginBottom' : 'marginRight';

    return (
        <div>
            {children.map((child, i) => {
                return React.cloneElement(child, {
                    style: {
                        [`${marginType}`]: (i !== children.length - 1) ? margin : null,
                        width: (mode === 'vertical') ? '100%' : null,
                    },
                    key: i,
                });
            })}
        </div>
    );
};

export default ButtonSet;
