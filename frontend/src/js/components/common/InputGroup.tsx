import * as React from 'react';

interface IInputGroupProps {
    children: React.ReactElement<any>[];
    id?: string;
}

type IInputGroup = (object: IInputGroupProps) => React.ReactElement<IInputGroupProps>;

const InputGroup: IInputGroup = ({children, id}) => {
    return (
        <div className='input-group'>
            <div className='input-group-addon'>
                {children[0]}
            </div>
            {id ? React.cloneElement(children[1], {id}) : children[1]}
        </div>
    );
};

export default InputGroup;
