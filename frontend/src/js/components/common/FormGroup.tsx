import * as React from 'react';
import { IValidator } from '../../common/helpers/validators';

interface IFormGroupProps {
    children: any | any[];
    label?: string;
    id?: string;
    className?: string;
    validators?: IValidator[];
    value?: string;
}

type IFormGroup = (object: IFormGroupProps) => React.ReactElement<IFormGroupProps>;

const FormGroup: IFormGroup = ({children, label, id, validators, value, className = null}) => {
    let formControl: React.ReactElement<any>, addons: React.ReactElement<any>[];

    if (Array.isArray(children)) {
        [formControl, ...addons] = children;
    } else {
        formControl = children;
    }

    return (
        <div className={`form-group ${className}`}>
            {label && <label className='control-label' htmlFor={id ? id : ''}>{label}</label>}
            {id ? React.cloneElement(formControl, {id}) : formControl}
            {addons && addons.map(addon => addon)}
            {
                validators &&
                validators
                    .map(validator => validator(value))
                    .map((error, i) => error && <label className='error-typing' key={i}>{error}</label>)
            }
        </div>
    );
};

export default FormGroup;
