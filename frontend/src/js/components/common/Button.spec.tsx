import * as React from 'react';
import * as renderer from 'react-test-renderer';
import 'jest';
import Button from './Button';

type IComponent = renderer.Renderer;
type ITree = renderer.ReactTestRendererJSON;

describe('<Button/>', () => {

    const fn: () => void = jest.fn();

    test('should has "btn btn-default" className when neither className nor bootstrapBtn is provided', () => {
        const component: IComponent = renderer.create(<Button onClick={fn}/>);
        const tree: ITree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('should has "btn btn-success" className when "success" bootstrapBtn is provided', () => {
        const component: IComponent = renderer.create(<Button onClick={fn} bootstrapBtn='success'/>);
        const tree: ITree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('should has "button" className when "button" className is provided', () => {
        const component: IComponent = renderer.create(<Button onClick={fn} className='button'/>);
        const tree: ITree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('should change type depending on provided one', () => {
        let component: IComponent = renderer.create(<Button onClick={fn}/>);
        let tree: ITree = component.toJSON();
        expect(tree).toMatchSnapshot();

        component = renderer.create(<Button type='submit' onClick={fn}/>);
        tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

});
