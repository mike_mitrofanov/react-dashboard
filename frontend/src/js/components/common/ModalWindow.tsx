import * as React from 'react';
import { Modal } from 'react-bootstrap';
import Button from './Button';

interface IModalWindowProps {
    showModal: boolean;
    title: string;
    onDecline?: () => void;
    onApply?: () => void;
    children?: any[];
    isFooterHidden?: boolean;
}

interface IModalWindowState {
    showModal: boolean;
}

export default class ModalWindow extends React.Component<IModalWindowProps, IModalWindowState> {

    constructor(props: IModalWindowProps) {
        super(props);

        this.state = {
            showModal: this.props.showModal ? this.props.showModal : false,
        };
    }

    public render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.onDecline}>
                <Modal.Header>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                {
                    (this.props.children) &&
                     <Modal.Body>
                         {this.props.children}
                     </Modal.Body>
                }
                {
                    !this.props.children &&
                    <Modal.Footer>
                        <Button
                            type={'button'}
                            onClick={this.props.onApply}
                            bootstrapBtn={'primary'}
                        >
                            Apply
                        </Button>
                        <Button
                            type={'button'}
                            onClick={this.props.onDecline}
                            bootstrapBtn={'primary'}
                        >
                            Cancel
                        </Button>
                    </Modal.Footer>
                }
            </Modal>
        );
    }

}
