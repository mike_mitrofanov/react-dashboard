/**
 * Created by developer on 30.05.17.
 */
import * as React from 'react';
import FormGroup from './FormGroup';
import InputGroup from './InputGroup';
import TextInput from './TextInput';
import { IValidator } from '../../common/helpers/validators';

interface IValidatedInputProps {
    id: string;
    value?: string;
    type: string;
    name: string;
    placeholder: string;
    label: string;
    className?: string;
    onChange?: (state: IValidatedField) => void;
    onBlur?: (value: string) => void;
    validators?: IValidator[];
    glyph?: string;
}

interface IInputState {
    value: string;
    status: string;
    glyph: string;
    isTouched: boolean;
}

export interface IValidatedField { // for form component
    value: string;
    isValid: boolean;
}

export default class ValidatedInput extends React.Component<IValidatedInputProps, IInputState> {

    public state: IInputState;

    constructor(props: any) {
        super(props);

        this.state = {
            value: this.props.value || '',
            status: '',
            glyph: '',
            isTouched: false,
        };
    }

    public componentWillReceiveProps (nextProps: IValidatedInputProps): void {
        this.setState({ value: nextProps.value });
    }

    public changeInputValue = (validators: IValidator[], value: string): void => {
        const _newState: IInputState = this._validateField(validators, value);
        this.setState({
            ..._newState,
            value,
        });
    };

    public enableInputValidation = (validators: IValidator[], value: string): void => {
        // here we have an old state, so need to send isTouched=true
        const _newState: IInputState = this._validateField(validators, value, true);
        this.setState({
            ..._newState,
            isTouched: true,
        });
    };

    /**
     * used in this.changeInputValue, this.enableInputValidation
     * Mark the field as valid/invalid, adds validation classes and icons
     * @return {IAuthField} this.state[field]
     */
    private _validateField = (validators: IValidator[], value: string, isTouched?: boolean): IInputState => {
        const validatedField: IInputState = JSON.parse(JSON.stringify(this.state));
        let isInputInvalid: boolean = false; // isInputInvalid collect errors
        validators.map(validator => (isInputInvalid = isInputInvalid || !!validator(value)));

        const _isTouched: boolean = (typeof isTouched !== 'undefined') ? isTouched : validatedField.isTouched;
        if (_isTouched && isInputInvalid) {
            validatedField.status = 'has-error';
            validatedField.glyph = 'form-control-feedback glyphicon glyphicon-remove';
        } else if (!isInputInvalid) {
            validatedField.status = 'has-success';
            validatedField.glyph = 'form-control-feedback glyphicon glyphicon-ok';
            validatedField.isTouched = true; // do not need to redraw, setState doesn't work
        }
        this.props.onChange({ value: value, isValid: !isInputInvalid });

        return validatedField;
    };

    private _renderTextInput (value: string, type: string, placeholder: string, validators: IValidator[]) {
        return (
            <TextInput
                type={type}
                name={name}
                value={value}
                onChange={this.changeInputValue.bind(this, validators)}
                placeholder={placeholder}
                onBlur={this.enableInputValidation.bind(this, validators)}
            />
        );
    }

    public render() {
        const { id, type, placeholder, label, className, validators } = this.props;
        const { status, value, glyph, isTouched } = this.state;
        const inputClassName: string = 'has-feedback' + (status ? ` ${status}` : '');

        return (
            <FormGroup
                label={label}
                id={id}
                className={inputClassName}
                validators={isTouched && validators || []}
                value={value}
            >
                {
                    className
                    &&
                    (
                        <InputGroup>
                            <span className={`glyphicon glyphicon-${className}`}/>
                            {this._renderTextInput(value, type, placeholder, validators)}
                        </InputGroup>
                    )
                    ||
                    this._renderTextInput(value, type, placeholder, validators)
                }
                {isTouched && <span className={glyph}/>}
            </FormGroup>
        );
    }
}
