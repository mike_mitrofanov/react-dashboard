import * as React from 'react';
import { connect } from 'react-redux';
import Link from '../Navigation/Link';
import RouteNames from '../../redux/routes/RouteNames';
import { IUser } from '../../common/models/IUser';
import { AuthService } from '../../services/app/Auth/AuthService';
import { iocContainer } from '../../ioc.config';
import { TYPES } from '../../ioc.types';
import { IMapStateToProps } from '../../common/IMapStateToProps';

interface IHeaderProps {
    user?: IUser;
}

class HeaderComponent extends React.Component<IHeaderProps, any> {

    private _authService: AuthService = iocContainer.get<AuthService>(TYPES.AuthService);

    constructor(props: IHeaderProps) {
        super(props);
    }

    public logout() {
        this._authService.logout();
    }

    public render() {
        const isUserExist: boolean = !!this.props.user;

        let authLink: React.ReactElement<any>;

        if (isUserExist) {
            authLink = <a href='javascript:void(0)' onClick={this.logout.bind(this)}>Logout</a>;
        } else {
            authLink = <Link href={RouteNames.auth}>Login</Link>;
        }

        return (
            <nav className='navbar navbar-inverse navbar-fixed-top topnav'>
                <div className='container topnav'>
                    <div className='navbar-header'>
                        <button
                            type='button'
                            className='navbar-toggle collapsed'
                            data-toggle='collapse'
                            data-target='#navbar'
                            aria-expanded='false'
                            aria-controls='navbar'
                        >
                            <span className='sr-only'>Toggle navigation</span>
                            <span className='icon-bar'/>
                            <span className='icon-bar'/>
                            <span className='icon-bar'/>
                        </button>
                        <Link href={RouteNames.index} className='navbar-brand topnav'>React Dashboard</Link>
                    </div>
                    <div id='navbar' className='collapse navbar-collapse'>
                        <ul className='nav navbar-nav navbar-right'>
                            <li>
                                <Link href={RouteNames.boards}>Boards</Link>
                            </li>
                            <li>
                                {authLink}
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }

}

const mapStateToProps: IMapStateToProps<IHeaderProps> = (state, props) => ({
    user: state.session.user,
});

export const Header: React.ComponentClass<IHeaderProps> = connect(mapStateToProps)(HeaderComponent);
