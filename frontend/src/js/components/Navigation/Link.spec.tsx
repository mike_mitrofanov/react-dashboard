import * as React from 'react';
import * as renderer from 'react-test-renderer';
import 'jest';
import { Link } from './Link';

describe('<Link/>', () => {

    const fn: () => void = jest.fn();

    test('should set a className when it provides', () => {
        let component: renderer.Renderer = renderer.create(<Link href='google.com' navigate={fn}/>);
        let tree: renderer.ReactTestRendererJSON = component.toJSON();
        expect(tree).toMatchSnapshot();

        component = renderer.create(<Link href='google.com' navigate={fn} className='link'/>);
        tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

});
