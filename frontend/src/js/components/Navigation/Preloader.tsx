/**
 * Created by Andrew on 11.05.2017.
 */
import * as React from 'react';

export default class Preloader extends React.Component<null, null> {

    public render() {
        return (
            <div className='preloader'>
                Push here some preloader!...
            </div>
        );
    }

}
