import * as React from 'react';
import * as redux from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

interface ILinkDispatch {
    navigate: (link: string) => void;
}

interface ILinkProps {
    title?: string;
    className?: string;
    children?: any;
    href: string;
    styles?: object;
}

export class Link extends React.Component<ILinkProps & ILinkDispatch, any> {

    constructor(props: ILinkProps & ILinkDispatch) {
        super(props);
    }

    public navigate = () => {
        this.props.navigate(this.props.href);
    };

    public render() {
        const {className, children} = this.props;
        const styles: {} = {...this.props.styles};

        return (
            <a
                href='javascript:void(0)'
                className={className ? className : null}
                onClick={this.navigate}
                style={styles}
            >
                {children && children}
            </a>
        );
    }

}

const mapStateToProps = (state: any, ownProps: ILinkProps): any => ({}); /* tslint:disable-line */

const mapDispatchToProps: any = (dispatch: redux.Dispatch<any>): ILinkDispatch => ({
    navigate: (link: string) => {
        dispatch(push(link));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
