import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Header } from '../Header/Header';
import Footer from '../Footer/Footer';
import IRouteDefinition from '../../redux/routes/IRouteDefinition';
import { RouteWithSubRoutes } from './RouteWithSubRoutes';

interface IProps extends RouteComponentProps<any> {
    routes: IRouteDefinition[];
}

export default class HeaderFooterRouteWrapper extends React.Component<IProps, any> {

    constructor(props: IProps) {
        super(props);
    }

    public render() {
        let routes: IRouteDefinition[] = this.props.routes;
        return (
            <div className='header-footer-layout'>
                <Header/>
                <div className='content'>
                    {routes ? routes.map((route, i) => (
                        <RouteWithSubRoutes key={i} route={route}/>
                    )) : null}
                </div>
                <Footer/>
            </div>
        );
    }

}

