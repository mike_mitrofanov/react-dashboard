import { Route, RouteComponentProps } from 'react-router';
import * as React from 'react';
import IRouteDefinition from '../../redux/routes/IRouteDefinition';
import { AuthDecorator } from 'redux-auth-wrapper';

interface IRouteProps {
    route: IRouteDefinition;
}

export class RouteWithSubRoutes extends React.Component<IRouteProps, any> {

    public applyMiddleware(
        component: React.ComponentClass<RouteComponentProps<any> | undefined>,
        middlewares: AuthDecorator<any>[]
    ): React.ComponentClass<RouteComponentProps<any> | undefined> {

        if (middlewares && middlewares.length > 0) {
            middlewares.forEach((middleware) => {
                component = middleware(component);
            });
        }

        return component;
    }

    public render() {
        let route: IRouteDefinition = this.props.route;
        let component: React.ComponentClass<RouteComponentProps<any> | undefined> = this.applyMiddleware(
            route.component,
            route.middlewares
        );
        return <Route exact={route.exact} path={route.path} render={(params) => {
            return React.createElement(component, {...params, routes: route.children});
        }}/>;
    }

}
