import * as React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import AppStore from '../redux/store';
import { AuthService } from '../services/app/Auth/AuthService';
import { iocContainer } from '../ioc.config';
import { TYPES } from '../ioc.types';
import { RouteDefinitions } from '../redux/routes/index';
import { RouteWithSubRoutes } from './Layouts/RouteWithSubRoutes';
import { Socket } from '../services/utils/socket';

interface IAppProps {

}

interface IAppState {

}

class App extends React.Component<IAppProps, IAppState> {

    private appStore: AppStore;
    private authService: AuthService;
    private socket: Socket;

    constructor(props: any) {
        super(props);
        this.appStore = iocContainer.get<AppStore>(TYPES.AppStore);
        this.socket = iocContainer.get<Socket>(TYPES.SocketClient);
        this.authService = iocContainer.get<AuthService>(TYPES.AuthService);
        this.authService.tryAutoLogin();
    }

    public render() {
        return (
            <Provider store={this.appStore.store}>
                <ConnectedRouter history={this.appStore.history}>
                    <div>
                        {RouteDefinitions.map((route, i) => (
                            <RouteWithSubRoutes key={i} route={route}/>
                        ))}
                    </div>
                </ConnectedRouter>
            </Provider>
        );
    }
}

export default App;
