import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { AuthService } from '../../services/app/Auth/AuthService';
import { iocContainer } from '../../ioc.config';
import { TYPES } from '../../ioc.types';
import FormGroup from '../common/FormGroup';
import Button from '../common/Button';
import ButtonSet from '../common/ButtonSet';
import { minLengthValidator, emailValidator, IValidator } from '../../common/helpers/validators';
import ValidatedInput, { IValidatedField } from '../common/ValidatedInput';

interface IAuthState {
    email?: IValidatedField;
    password?: IValidatedField;
    regSuccess?: boolean | null;
    regMessage?: string;
}

export default class Auth extends React.Component<RouteComponentProps<null>, IAuthState> {

    private _authService: AuthService = iocContainer.get<AuthService>(TYPES.AuthService);

    private initialFieldState: IValidatedField = {
        value: '',
        isValid: false,
    };

    constructor(props: any) {
        super(props);

        this.state = {
            regSuccess: null,
            regMessage: '',
            email: JSON.parse(JSON.stringify(this.initialFieldState)),
            password: JSON.parse(JSON.stringify(this.initialFieldState)),
        };
    }

    private _checkValidStatus = (): boolean => {
        return this.state.email.isValid && this.state.password.isValid;
    };

    public onChangeField (field: string, fieldState: IValidatedField) {
        this.setState({
            ...this.state,
            [field]: fieldState,
        });
    }

    public shouldLogin = (e: any): void => {
        if (e.keyCode === 13) {
            this.login();
        }
    };

    public login = (): void => {
        if (this._checkValidStatus()) {
            this._authService.login(this.state.email.value, this.state.password.value);
        }
    };

    public register = (): void => {
        if (this._checkValidStatus()) {
            this._authService.register(this.state.email.value, this.state.password.value)
                .then(res => {
                    if (res.data.success) {
                        this.registerResult(true, 'Registration completed successfully');
                        return;
                    }

                    this.registerResult(false);
                })
                .catch(err => {
                    if (err.response.data.msg.indexOf('duplicate') > -1) {
                        this.registerResult(false, 'A user with the same email already exists');
                        return;
                    }

                    this.registerResult(false);
                });
        }
    };

    public registerResult = (isSuccess: boolean, regMessage: string = 'Something went wrong'): void => {
        if (isSuccess) {
            const init: IValidatedField = this.initialFieldState;
            this.setState({email: init, password: init, regSuccess: isSuccess, regMessage});
        } else {
            this.setState({regSuccess: isSuccess, regMessage});
        }
    };

    public render() {
        const {regSuccess, regMessage} = this.state;

        const emailValidators: IValidator[] = [emailValidator];
        const passwordValidators: IValidator[] = [minLengthValidator.bind(null, 3)];

        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-sm-12 col-md-4 col-md-offset-4'>
                        <form className='auth-form' onKeyDown={this.shouldLogin}>
                            <ValidatedInput
                                id={'auth-email'}
                                type={'email'}
                                name={'email'}
                                placeholder={'E-mail'}
                                label={'E-mail'}
                                className={'envelope'}
                                validators={emailValidators}
                                onChange={this.onChangeField.bind(this, 'email')}
                            />
                            <ValidatedInput
                                id={'auth-password'}
                                type={'password'}
                                name={'password'}
                                placeholder={'Password'}
                                label={'Password'}
                                className={'lock'}
                                validators={passwordValidators}
                                onChange={this.onChangeField.bind(this, 'password')}
                            />
                            <FormGroup>
                                <ButtonSet>
                                    <Button
                                        type='button'
                                        bootstrapBtn={this._checkValidStatus() && 'primary'}
                                        onClick={this.login}
                                    >
                                        Login
                                    </Button>
                                    <Button
                                        type='button'
                                        bootstrapBtn={this._checkValidStatus() && 'success'}
                                        onClick={this.register}
                                        position={'right'}
                                    >
                                        Register
                                    </Button>
                                </ButtonSet>
                            </FormGroup>
                            {regSuccess !== null && (
                                <div style={{color: regSuccess ? '#3c763d' : '#a94442'}}>{regMessage}</div>
                            )}
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
