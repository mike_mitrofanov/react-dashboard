import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import BoardItem, { IBoardItemProps } from './BoardItem'; /* tslint:disable-line */
import { IBoard } from '../../common/models/IBoard';
import { HTMLAttributes } from 'react'; /* tslint:disable-line */

function setup(spy: () => any) {
    const board: IBoard = {
        _id: '123',
        name: 'Board Name',
        owner: {
            _id: null,
            email: 'email@example.com',
        },
        columns: [],
    };

    return shallow(<BoardItem board={board} removeBoard={spy}/>);
}

describe('<BoardItem/>', () => {

    const spy: () => any = jest.fn();
    const wrapper: ShallowWrapper<IBoardItemProps, null> = setup(spy);

    it('should provide correct link', () => {
        expect(wrapper.find('.board-item').prop('href')).toEqual('/boards/123');
    });

    it('should have title', () => {
        expect(wrapper.find('.board-title').text()).toEqual('Board Name');
    });

    it('should have owner info', () => {
        expect(wrapper.find('.board-owner').text()).toEqual('email@example.com');
    });

    describe('handle removing board', () => {

        const removeBtn: ShallowWrapper<HTMLAttributes<any>, any> = wrapper.find('.remove-board-icon');

        it('should contain remove button', () => {
            expect(removeBtn.length).toEqual(1);
        });

        it('should call removeBoard function on click', () => {
            removeBtn.simulate('click', {stopPropagation: () => false});
            expect(spy).toHaveBeenCalled();
        });

        it('should pass board id to removeBoard function', () => {
            removeBtn.simulate('click', {stopPropagation: () => false});
            expect(spy).toHaveBeenCalledWith('123');
        });

    });

});
