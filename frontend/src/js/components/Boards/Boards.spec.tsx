import 'reflect-metadata';
import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as sinon from 'sinon';
import { Boards, IBoardsProps, IBoardsState } from './Boards'; /* tslint:disable-line */
import { Provider } from 'react-redux';
import { iocContainer } from '../../ioc.config';
import AppStore from '../../redux/store/index'; /* tslint:disable-line */
import { TYPES } from '../../ioc.types';
import { API_URL } from '../../services/constants';
import data from '../../../../mocks/data/boards';
import ModalWindow from '../common/ModalWindow';
import { HTMLAttributes } from 'react'; /* tslint:disable-line */
import { SinonFakeServer } from 'sinon';

describe('<Boards/>', () => {

    const wrapper: ReactWrapper<IBoardsProps, IBoardsState> = mount(
        <Provider store={iocContainer.get<AppStore>(TYPES.AppStore).store}>
            <Boards boards={data}/>
        </Provider>
    );

    const server: SinonFakeServer = sinon.fakeServer.create();
    server.respondWith('GET', `${API_URL}/boards`, [
        200, {}, JSON.stringify(data),
    ]);

    beforeEach(() => server.respond());

    afterEach(() => server.restore());

    it('should render 2 boards', () => {
        expect(wrapper.find('.board-item').not('.new-board').length).toEqual(2);
    });

    it('should render NewBoard component', () => {
        expect(wrapper.find('.new-board').length).toEqual(1);
    });

    describe('deleting board', () => {

        const removeBtns: ReactWrapper<HTMLAttributes<any>, any> = wrapper.find('.remove-board-icon');

        it('should contain 2 remove board buttons', () => {
            expect(removeBtns.length).toEqual(2);
        });

        it('should show modal window after clicking on remove board button', () => {
            expect(wrapper.find(ModalWindow).props().showModal).toEqual(false);
            removeBtns.first().simulate('click');
            expect(wrapper.find(ModalWindow).props().showModal).toEqual(true);
        });

    });

});
