import * as React from 'react';
import { connect } from 'react-redux';
import { IAppState } from '../../redux/store/initialState';
import { BoardsService } from '../../services/app/Board/BoardsService';
import { iocContainer } from '../../ioc.config';
import { TYPES } from '../../ioc.types';
import { IBoard } from '../../common/models/IBoard';
import { denormalize } from 'normalizr';
import { BoardsSchema } from '../../common/schema/DataSchema';
import { Col, Row } from 'react-bootstrap';
import randomColor = require('random-material-color'); /* tslint:disable-line */
import NewBoard from './NewBoard';
import ModalWindow from '../common/ModalWindow';
import BoardItem from './BoardItem';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import DndService from '../../services/DndService';
import { AuthService } from '../../services/app/Auth/AuthService';
import { IUser } from '../../common/models/IUser';
import { swapBoardsAction } from './boardsActions';

export interface IBoardsProps {
    boards: IBoard[];
}

export interface IBoardsState {
    showDeleteModal?: boolean;
    boardToRemove?: string | null;
    boards?: IBoard[];
}

@DragDropContext(HTML5Backend)
export class Boards extends React.Component<IBoardsProps, IBoardsState> {

    private _boardsService?: BoardsService = iocContainer.get<BoardsService>(TYPES.BoardsService);
    private _authService?: AuthService = iocContainer.get<AuthService>(TYPES.AuthService);

    constructor(props: IBoardsProps) {
        super(props);

        this.state = {
            showDeleteModal: false,
            boardToRemove: null,
            boards: this.props.boards,
        };
    }

    public componentWillReceiveProps? (nextProps: IBoardsProps) {
        this.setState({ boards: nextProps.boards });
    }

    public componentDidMount? () {
        this._boardsService.loadAll();
    }

    public removeBoard? = (id: string): void => {
        this.setState({
            showDeleteModal: true,
            boardToRemove: id,
        });
    };

    public declineDeleting? = (): void => {
        this.setState({
            showDeleteModal: false,
            boardToRemove: null,
        });
    };

    public applyDeleting? = (): void => {
        this._boardsService.removeBoard(this.state.boardToRemove);
        this.setState({
            showDeleteModal: false,
            boardToRemove: null,
        });
    };

    public saveBoardOrder? = (newState: IBoardsState): void => {
        const user: IUser = JSON.parse(localStorage.getItem('user'));
        if (!user.settings) {
            user.settings = {};
        }
        user.settings.boardIds = newState.boards.map(board => board._id);
        this._authService.editUser(user, swapBoardsAction.bind(null, newState.boards));
    };

    public render () {
        return (
            <div className='page boards container'>
                <h1>Your boards</h1>
                <hr/>
                <ModalWindow
                    showModal={this.state.showDeleteModal}
                    title={'Do you want to remove this board?'}
                    onDecline={this.declineDeleting.bind(this)}
                    onApply={this.applyDeleting.bind(this)}
                />
                <Row>
                    <Col lg={3} md={4} sm={6} xs={12}>
                        <NewBoard/>
                    </Col>
                    {this.state.boards.map((board: IBoard, i: number) => (
                        <BoardItem
                            board={board}
                            removeBoard={this.removeBoard}
                            key={board._id}
                            index={i}
                            id={board._id}
                            moveCard={DndService.moveCard.bind(
                                this,
                                'boards',
                                this.state,
                                this.saveBoardOrder.bind(this)
                            )}
                        />
                    ))}
                </Row>
            </div>
        );
    }

}

/* tslint:disable-next-line:typedef */
const mapStateToProps = (state: IAppState, ownProps: any): IBoardsProps => ({
    boards: denormalize(state.entities.ids.boards, BoardsSchema, state.entities.normalized),
});

export default connect(mapStateToProps)(Boards);
