/**
 * Created by developer on 26.05.17.
 */
import { SWAP_BOARDS } from '../../common/actionNames';
import { IBoard } from '../../common/models/IBoard';

export function swapBoardsAction (boards: IBoard[], dispatch: (obj: {}) => void) {
    dispatch({
        type: SWAP_BOARDS,
        payload: {
            data: boards,
        },
    });
}
