import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BoardsService } from '../../services/app/Board/BoardsService';
import { iocContainer } from '../../ioc.config';
import { TYPES } from '../../ioc.types';
import { Button, FormControl, FormGroup } from 'react-bootstrap';

interface INewBoardState {
    boardName: string;
    isActive: boolean;
}

export default class NewBoard extends React.Component<null, INewBoardState> {

    private _boardsService: BoardsService = iocContainer.get<BoardsService>(TYPES.BoardsService);

    private initialState: INewBoardState = {
        boardName: '',
        isActive: false,
    };

    constructor(props: any) {
        super(props);

        this.state = this.initialState;

        document.addEventListener('click', this.handleOuterClick);
    }

    public componentWillUnmount() {
        document.removeEventListener('click', this.handleOuterClick);
    }

    public reset = (): void => {
        this.setState(this.initialState);
    };

    public handleOuterClick = (e: any): void => {
        const node: Element = ReactDOM.findDOMNode(this);

        if (!node.contains(e.target)) {
            this.reset();
        }
    };

    public showForm = (): void => {
        this.setState({isActive: true});
    };

    public handleChange = (e: any): void => {
        const boardName: string = e.target.value;

        if (boardName !== this.state.boardName) {
            this.setState({boardName});
        }
    };

    public createBoard = (e: any): void => {
        e.stopPropagation();

        if (!e.keyCode || e.keyCode === 13) {
            const boardName: string = this.state.boardName;

            if (boardName) {
                this._boardsService.addBoard(boardName);
                this.reset();
            }
        }
    };

    public render() {
        const {boardName, isActive} = this.state;

        return (
            <div className={`board-item new-board ${isActive ? 'active' : ''}`} onClick={this.showForm}>
                <FormGroup>
                    <FormControl
                        type='text'
                        placeholder='Enter board name'
                        value={boardName}
                        onChange={this.handleChange}
                        onKeyDown={this.createBoard}
                    />
                </FormGroup>
                <Button bsStyle='success' onClick={this.createBoard}>Add new board</Button>
            </div>
        );
    }

}
