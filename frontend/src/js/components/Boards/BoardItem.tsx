import * as React from 'react';
import Link from '../Navigation/Link';
import RouteNames from '../../redux/routes/RouteNames';
import { IBoard } from '../../common/models/IBoard';
import { DragSource, DropTarget, DndComponent } from 'react-dnd';
import DndService from '../../services/DndService';
import RemoveIcon from '../common/RemoveIcon';

export interface IBoardItemProps {
    board?: IBoard;
    removeBoard?: (id: string) => void;
    key?: string;
    index?: number;
    id?: string;
    moveCard?: any;
    isDragging?: boolean;
    connectDragSource?: any;
    connectDropTarget?: any;
}

@DropTarget('card', DndService.dropTarget(), connect => ({
    connectDropTarget: connect.dropTarget(),
}))
@DragSource('card', DndService.dragSource(), (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}))
export default class BoardItem extends React.Component<IBoardItemProps, any> {

    constructor(props: IBoardItemProps) {
        super(props);
    }

    private removeBoard? = (e: any) => {
        e.stopPropagation();
        this.props.removeBoard(this.props.board._id);
    };

    public render() {
        const {_id, name, owner} = this.props.board;
        const {isDragging, connectDragSource, connectDropTarget} = this.props;
        const opacity: number = isDragging ? 0 : 1;

        let ownerInfo: string = '';

        if (owner.firstName && owner.lastName) {
            ownerInfo = `${owner.firstName} ${owner.lastName}`;
        } else {
            ownerInfo = owner.email;
        }

        return connectDragSource(connectDropTarget(
            <div style={{opacity}} className={'col-lg-3 col-md-4 col-sm-6 col-xs-12'}>
                <Link href={`${RouteNames.boards}/${_id}`} className='board-item'>
                    <div className='board-title text-overflow'>{name}</div>
                    <div className='board-owner text-overflow'>{ownerInfo}</div>
                    <RemoveIcon onClick={this.removeBoard}/>
                </Link>
            </div>
        ));
    }

}
