import { schema, Schema } from 'normalizr';

export const TicketSchema: Schema = new schema.Entity('tickets', {}, {idAttribute: '_id'});

export const ColumnSchema: Schema = new schema.Entity('columns', {
    tickets: [TicketSchema],
}, {idAttribute: '_id'});

export const UserSchema: Schema = new schema.Entity('users', {}, {idAttribute: '_id'});

export const BoardSchema: Schema = new schema.Entity('boards', {
    columns: [ColumnSchema],
    owner: UserSchema,
    assigners: [UserSchema],
}, {idAttribute: '_id'});

export const BoardsSchema: Schema = new schema.Array(BoardSchema);

/* tslint:disable-next-line:typedef */
export const CascadeSchemaDependencies = {
    // define behavior of deleting items: remove all children items, remove self from parents fields
    boards: {
        parents: [
            // {
            //     entity: 'user',
            //     field: 'owner',
            //     type: 'array',
            // }
        ],
        children: [
            {
                entityType: 'columns',
                field: 'columns',
                type: 'array',
            },
        ],
    },
    columns: {
        parents: [
            {
                entityType: 'boards',
                field: 'columns',
                type: 'array',
            },
        ],
        children: [
            {
                entityType: 'tickets',
                field: 'tickets',
                type: 'array',
            },
        ],
    },
    tickets: {
        parents: [
            {
                entityType: 'columns',
                field: 'tickets',
                type: 'array',
            },
        ],
    },
};
