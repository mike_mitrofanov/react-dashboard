import { Action } from 'redux';

export interface IEntityDeleteAction extends Action {
    payload: {
        entityName: string,
        ids: string[],
        parentScope: any,
    };
}
