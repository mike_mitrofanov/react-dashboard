export interface IEntityNode {
    entities: object;
    ids: string[];
}
