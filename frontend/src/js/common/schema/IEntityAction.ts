import { Action } from 'redux';
import { IEntityNode } from './IEntityNode';

export interface IEntityAction extends Action {
    payload: {
        data: IEntityNode,
        entityName: string,
        mergeStrategy?: string,
    };
}
