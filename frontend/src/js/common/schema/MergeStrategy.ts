export const MERGE_STRATEGY: any = {
    REWRITE: 'REWRITE', // rewrite object
    EXTEND: 'EXTEND', // concat object fields
    DIFF: 'DIFF', // remove fields from store object
    SWAP: 'SWAP', // just change places of item after dnd
};
