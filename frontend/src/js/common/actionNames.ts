export const SET_SESSION_USER: string = 'SET_SESSION_USER';
export const SET_USER_LOADING: string = 'SET_USER_LOADING';

export const ENTITY_UPDATE: string = 'ENTITY_UPDATE';
export const ENTITY_DELETE: string = 'ENTITY_DELETE';
export const SWAP_BOARDS: string = 'SWAP_BOARDS';

export const USER_ASSIGNED_TO_TICKET: string = 'USER_ASSIGNED_TO_TICKET';
export const USER_UNASSIGNED_FROM_TICKET: string = 'USER_UNASSIGNED_FROM_TICKET';
export const USER_ASSIGNED_TO_BOARD: string = 'USER_ASSIGNED_TO_BOARD';
export const USER_UNASSIGNED_FROM_BOARD: string = 'USER_UNASSIGNED_FROM_BOARD';

export const socketMessageTypes: {} = [
    USER_ASSIGNED_TO_TICKET,
    USER_UNASSIGNED_FROM_TICKET,
    USER_ASSIGNED_TO_BOARD,
    USER_UNASSIGNED_FROM_BOARD,
].reduce((accum: any, msg: string) => {
    accum[msg] = msg;
    return accum;
}, {});
