import { IAppState } from '../redux/store/initialState';

export type IMapStateToProps<P> = (state: IAppState, ownProps: P) => P;
