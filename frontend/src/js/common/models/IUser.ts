export interface IUser {
    _id: string;
    email: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    thumbnail?: string;
    password?: string;
    settings?: {
        boardIds?: string[],
    };
}
