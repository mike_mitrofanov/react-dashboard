import { IColumn } from './IColumn';
import { IUser } from './IUser';

export interface IBoard {
    _id: string;
    name: string;
    owner: IUser;
    columns?: IColumn[];
}
