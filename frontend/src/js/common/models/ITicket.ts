import { IUser } from './IUser';

export interface ITicket {
    _id: string;
    title: string;
    creator: string | IUser;
    comments: any[];
    assigners: any[];
    description: string;
}
