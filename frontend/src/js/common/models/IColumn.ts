import { ITicket } from './ITicket';

export interface IColumn {
    _id: string;
    name: string;
    tickets: ITicket[];
}
