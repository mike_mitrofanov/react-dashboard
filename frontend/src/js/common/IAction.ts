import { Action } from 'redux';

export interface IAction<T> extends Action {
    payload: T;
}

export interface INormalizedAction<T> extends Action {
    payload: {
        entities: T[],
        ids: string[],
    };
}
