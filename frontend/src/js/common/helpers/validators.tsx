/**
 * Created by developer on 26.05.17.
 */
import CONFIG from '../../config/index';

export interface IValidator {
    (limit: number | string, value?: string): string;
}

export function minLengthValidator (limit: number, value: string): string {
    return value.length < limit && `Minimal length is ${limit}` || '';
}

export function maxLengthValidator (limit: number, value: string): string {
    return value.length > limit && `Maximal length is ${limit}` || '';
}

export function minValidator (limit: number, value: string): string {
    return numberValidator(value) || (+value < limit && `Minimal value is ${limit}`) || '';
}

export function maxValidator (limit: number, value: string): string {
    return numberValidator(value) || (+value > limit && `Maximal value is ${limit}`) || '';
}

// type validators

export function numberValidator (value: string): string {
    return isNaN(+value) && 'Must be a number' || '';
}

export function emailValidator (value: string): string {
    return !value.match(CONFIG.REGEXP.email) && 'Must be an email' || '';
}
